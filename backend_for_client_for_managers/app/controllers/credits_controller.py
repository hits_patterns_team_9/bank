import requests
from flask import Blueprint, jsonify, request
from backend_for_client_for_managers.settings import *

credits_managers_bp = Blueprint('/credits', __name__, url_prefix=BACKEND_FOR_MANAGERS_URL_PREFIX)

@credits_managers_bp.route('/credits', methods=['GET'])
def get_all_credits_managers():
    response = requests.get(
        f'http://{BACKEND_HOST}:{BACKEND_CORE_PORT}/{BACKEND_CORE_URL_PREFIX}/credits')

    if response.status_code == 200:
        return jsonify(response.json()), 200
    else:
        return jsonify({'error': 'Error in the downstream request'}), 500

@credits_managers_bp.route('/credits/<string:credit_number>', methods=['GET'])
def get_credit_by_credit_number(credit_number):
    response = requests.get(
        f'http://{BACKEND_HOST}:{BACKEND_CORE_PORT}/{BACKEND_CORE_URL_PREFIX}/credits/{credit_number}')

    if response.status_code == 200:
        return jsonify(response.json()), 200
    else:
        return jsonify({'error': 'Error in the downstream request'}), 500

@credits_managers_bp.route('/credits/<string:credit_number>/history', methods=['GET'])
def get_credit_history(credit_number):
    response = requests.get(
        f'http://{BACKEND_HOST}:{BACKEND_CORE_PORT}/{BACKEND_CORE_URL_PREFIX}/credits/{credit_number}/history')

    if response.status_code == 200:
        return jsonify(response.json()), 200
    else:
        return jsonify({'error': 'Error in the downstream request'}), 500



'''
/api/backend_for_managers/credits
/api/backend_for_managers/credits/{credit_number}
/api/backend_for_managers/credits/{credit_number}/history
'''