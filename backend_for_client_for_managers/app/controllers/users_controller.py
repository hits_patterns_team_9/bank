import requests
from flask import Blueprint, jsonify, request
from backend_for_client_for_managers.settings import *

users_managers_bp = Blueprint('/users', __name__, url_prefix=BACKEND_FOR_MANAGERS_URL_PREFIX)

@users_managers_bp.route('/users/<uuid:user_id>', methods=['PATCH'])
def change_user_roles(user_id):
    data = request.get_json()
    request_roles = data.get('roles')

    request_roles = {
        'roles': request_roles
    }

    response = requests.patch(
        f'http://{BACKEND_HOST}:{BACKEND_USER_PROCESSING_PORT}/{BACKEND_USER_PROCESSING_URL_PREFIX}/users/{user_id}/roles',
        json=request_roles)

    if response.status_code == 200:
        return jsonify(response.json()), 200
    else:
        return jsonify({'error': 'Error in the downstream request'}), 500

'''
/api/backend_for_managers/users/{user_id}
'''