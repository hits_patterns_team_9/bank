from flask import Flask
from flask import request

from flask_cors import CORS
from backend_for_client_for_managers.app.controllers.credits_controller import credits_managers_bp
from backend_for_client_for_managers.app.controllers.users_controller import users_managers_bp
from backend_for_client_for_managers.settings import *

import logging

app = Flask(__name__)
CORS(app)

app.logger.setLevel(logging.DEBUG)


@app.before_request
def log_request_info():
    app.logger.debug(f'\nHeaders: \n{request.headers}\nBody: \n{request.get_data().decode()}')


@app.route('/')
def hello():
    return 'Backend for managers - backend app'


app.register_blueprint(credits_managers_bp)
app.register_blueprint(users_managers_bp)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=BACKEND_FOR_MANAGERS_PORT)
