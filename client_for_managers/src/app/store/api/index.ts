import { AppApi, UsersApi } from "../../../shared";

export const apiReducers = {
  [AppApi.reducerPath]: AppApi.reducer,
  [UsersApi.reducerPath]: UsersApi.reducer,
};

export const apiMiddleware = [AppApi.middleware, UsersApi.middleware];
