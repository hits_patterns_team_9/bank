export type Credit = {
  additional_conditions: string;
  commission: number;
  created_date: string;
  credit_id: string;
  credit_name: string;
  credit_number: string;
  credit_sum: number;
  credit_type: string;
  expire_date: string;
  interest_rate: number;
  money_type: string;
  service_price: number;
  user_id: string;
  withdraw_high_limit_per_day: number;
  withdraw_high_limit_per_month: number;
  withdraw_low_limit_per_day: number;
  withdraw_low_limit_per_month: number;
};

export type CreditHistory = {
  created_at: string;
  credit: Credit;
  history_id: string;
  operation_name: string;
  operation_sum: number;
  operation_type: string;
};
