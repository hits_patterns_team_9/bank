export const CREDITS_LIST = "/credits";
export const CREDIT = (number: string = ":number") => `/credit/${number}`;
