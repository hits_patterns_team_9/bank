import {
  BaseQueryFn,
  FetchArgs,
  FetchBaseQueryError,
  FetchBaseQueryMeta,
  fetchBaseQuery,
} from "@reduxjs/toolkit/query";

export const baseQuery = fetchBaseQuery({
  baseUrl: import.meta.env.VITE_API_URL,
  prepareHeaders(headers) {
    return headers;
  },
});
export const usersQuery = fetchBaseQuery({
  baseUrl: import.meta.env.VITE_API_USERS_URL,
  prepareHeaders(headers) {
    return headers;
  },
});

export const fetchBaseQueryRefreshToken: BaseQueryFn<
  FetchArgs,
  unknown,
  FetchBaseQueryError,
  object,
  FetchBaseQueryMeta
> = async (args, api, extraOptions) => {
  const result = await baseQuery(args, api, extraOptions);

  return result;
};

export const fetchUsersQuery: BaseQueryFn<
  FetchArgs,
  unknown,
  FetchBaseQueryError,
  object,
  FetchBaseQueryMeta
> = async (args, api, extraOptions) => {
  const result = await usersQuery(args, api, extraOptions);

  return result;
};
