import { injectToApi } from "../..";
import { Credit, CreditHistory } from "../../..";

export const getBankAccountsApi = injectToApi({
  endpoints: (builder) => ({
    creditsList: builder.query<{ credits: Array<Credit> }, void>({
      query: () => ({
        url: `/credits`,
        method: "GET",
      }),
      providesTags: ["GET_CREDITS_LIST"],
    }),

    creditInfo: builder.query<{ credit: Credit }, { credit_number: string }>({
      query: ({ credit_number }) => ({
        url: `/credits/${credit_number}`,
        method: "GET",
      }),
      providesTags: ["GET_CREDIT_INFO"],
    }),
    creditHistory: builder.query<
      { credit: Array<CreditHistory> },
      { credit_number: string }
    >({
      query: ({ credit_number }) => ({
        url: `/credits/${credit_number}/history`,
        method: "GET",
      }),
      providesTags: ["GET_CREDIT_HISTORY"],
    }),
  }),
});

export const {
  useCreditsListQuery,
  useCreditInfoQuery,
  useCreditHistoryQuery,
} = getBankAccountsApi;
