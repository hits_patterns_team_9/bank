import { injectToUsersApi } from "../..";
import { User } from "../../../lib/entities/User";

export const getUsersApi = injectToUsersApi({
  endpoints: (builder) => ({
    userInfo: builder.query<{ users: User }, { user_id: string }>({
      query: ({ user_id }) => ({
        url: `/users/${user_id}`,
        method: "GET",
      }),
    }),
    users: builder.query<{ users: Array<User> }, void>({
      query: () => ({
        url: `/users`,
        method: "GET",
      }),
    }),
  }),
});

export const { useUserInfoQuery, useUsersQuery } = getUsersApi;
