import React from "react";
import ReactDOM from "react-dom/client";
import { AppRoutes } from "./app/index.tsx";
import "./main.css";
import { Provider } from "react-redux";
import { store } from "./app/store/index.ts";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <Provider store={store}>
      <AppRoutes />
    </Provider>
  </React.StrictMode>
);
