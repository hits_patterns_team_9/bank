import { injectToApi } from "../../../../shared";

export type CreateCreditBody = {
  credit_name: string;
  credit_sum: number;
  credit_type: string;
  money_type: string;
  service_price: number;
  withdraw_low_limit_per_day: number;
  withdraw_high_limit_per_day: number;
  withdraw_low_limit_per_month: number;
  withdraw_high_limit_per_month: number;
  commission: number;
  interest_rate: number;
  created_date: string;
  expire_date: string;
  additional_conditions: string;
  user_id: string;
};

export const createCreditApi = injectToApi({
  endpoints: (builder) => ({
    createCredit: builder.mutation<void, CreateCreditBody>({
      query: (body) => ({
        url: `/credits`,
        method: "POST",
        body: { ...body, user_id: "09630388-fa26-4674-a513-e5361203fb08" },
      }),
      invalidatesTags: ["GET_CREDITS_LIST"],
    }),
  }),
});

export const { useCreateCreditMutation } = createCreditApi;
