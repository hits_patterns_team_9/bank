import { Button, Form, Input, InputNumber, Modal, Select } from "antd";

import { CreateCreditBody, useCreateCreditMutation } from "./api";
import { useState } from "react";
import { useUsersQuery } from "../../../shared";

export const CreateCredit = () => {
  const [createCredit] = useCreateCreditMutation();

  const [open, setOpen] = useState<boolean>(false);
  const createCreditHandler = (body: CreateCreditBody) => {
    createCredit(body)
      .unwrap()
      .then(() => {
        setOpen(false);
      });
  };

  const { data } = useUsersQuery();
  return (
    <>
      <Modal
        title={"Создать счёт"}
        open={open}
        onCancel={() => {
          setOpen(false);
        }}
        footer={null}
      >
        <Form<CreateCreditBody>
          onFinish={createCreditHandler}
          layout="vertical"
        >
          <Form.Item<CreateCreditBody>
            name="credit_name"
            label="Название"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<CreateCreditBody>
            name="credit_sum"
            label="credit_sum"
            rules={[{ required: true }]}
          >
            <InputNumber />
          </Form.Item>
          <Form.Item<CreateCreditBody>
            name="credit_type"
            label="credit_type"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<CreateCreditBody>
            name="money_type"
            label="money_type"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<CreateCreditBody>
            name="service_price"
            label="service_price"
            rules={[{ required: true }]}
          >
            <InputNumber />
          </Form.Item>
          <Form.Item<CreateCreditBody>
            name="withdraw_low_limit_per_day"
            label="withdraw_low_limit_per_day"
            rules={[{ required: true }]}
          >
            <InputNumber />
          </Form.Item>
          <Form.Item<CreateCreditBody>
            name="withdraw_high_limit_per_day"
            label="withdraw_high_limit_per_day"
            rules={[{ required: true }]}
          >
            <InputNumber />
          </Form.Item>
          <Form.Item<CreateCreditBody>
            name="withdraw_low_limit_per_month"
            label="withdraw_low_limit_per_month"
            rules={[{ required: true }]}
          >
            <InputNumber />
          </Form.Item>
          <Form.Item<CreateCreditBody>
            name="withdraw_high_limit_per_month"
            label="withdraw_high_limit_per_month"
            rules={[{ required: true }]}
          >
            <InputNumber />
          </Form.Item>
          <Form.Item<CreateCreditBody>
            name="commission"
            label="commission"
            rules={[{ required: true }]}
          >
            <InputNumber />
          </Form.Item>
          <Form.Item<CreateCreditBody>
            name="interest_rate"
            label="interest_rate"
            rules={[{ required: true }]}
          >
            <InputNumber />
          </Form.Item>
          <Form.Item<CreateCreditBody>
            name="created_date"
            label="created_date"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<CreateCreditBody>
            name="expire_date"
            label="expire_date"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<CreateCreditBody>
            name="additional_conditions"
            label="additional_conditions"
          >
            <Input />
          </Form.Item>
          <Form.Item<CreateCreditBody>
            name="user_id"
            label="user_id"
            rules={[{ required: true }]}
          >
            <Select
              options={data?.users.map((user) => {
                return {
                  value: user.user_id,
                  label: user.username,
                };
              })}
            />
          </Form.Item>
          <Form.Item>
            <Button htmlType="submit">Создать</Button>
          </Form.Item>
        </Form>
      </Modal>
      <Button
        onClick={() => {
          setOpen(true);
        }}
      >
        создать счёт
      </Button>
    </>
  );
};
