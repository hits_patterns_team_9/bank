import { Flex, Typography } from "antd";
import { CREDIT, useCreditsListQuery } from "../shared";
import { CreditistItem } from "../entities";
import { Link } from "react-router-dom";
import { CreateCredit } from "../features/Credit/CreateCredit";

const CreditsList = () => {
  const { data } = useCreditsListQuery();

  return (
    <>
      <Flex justify="space-between" align="center">
        <Typography.Title style={{ margin: 0 }}>Все счета</Typography.Title>
        <CreateCredit />
      </Flex>

      <Flex gap={16} vertical>
        {data?.credits?.map((credit) => {
          return (
            <Link key={credit.credit_id} to={CREDIT(credit.credit_number)}>
              <CreditistItem credit={credit} />
            </Link>
          );
        })}
      </Flex>
    </>
  );
};

export default CreditsList;
