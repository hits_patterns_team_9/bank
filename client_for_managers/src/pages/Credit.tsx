import { Flex, Spin, Typography } from "antd";
import { useCreditHistoryQuery, useCreditInfoQuery } from "../shared";
import { useParams } from "react-router-dom";

import { CreditistItem } from "../entities";

const { Text } = Typography;
const Credit = () => {
  const { number } = useParams();
  const { data, isFetching } = useCreditInfoQuery({ credit_number: number! });
  const { data: history, isFetching: isHistoryFetching } =
    useCreditHistoryQuery({ credit_number: number! });

  if (!data) {
    return null;
  }
  return (
    <Spin spinning={isFetching || isHistoryFetching}>
      <Flex justify="space-between" align="center">
        <Typography.Title style={{ margin: 0 }}>Счет</Typography.Title>
      </Flex>

      <CreditistItem credit={data.credit} />

      <Typography.Title>История:</Typography.Title>
      <Flex vertical gap={16}>
        {history?.credit?.map(
          ({ history_id, operation_name, operation_type, operation_sum }) => {
            return (
              <Flex vertical key={history_id}>
                <Text>Название операции: {operation_name}</Text>
                <Text>Сумма: {operation_sum}</Text>
                <Text>Тип: {operation_type}</Text>
              </Flex>
            );
          }
        )}
      </Flex>
    </Spin>
  );
};

export default Credit;
