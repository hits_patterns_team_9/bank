from backend_user_processing.app.models.users_model import User
from backend_user_processing.app.models.roles_model import Role
from backend_user_processing.app.models.users_roles_model import UserRole


def drop_all_tables():
    User.delete().execute()
    User.drop_table(User, cascade=True)
    Role.delete().execute()
    Role.drop_table(Role, cascade=True)
    UserRole.delete().execute()
    UserRole.drop_table(UserRole, cascade=True)


drop_all_tables()
