from backend_user_processing.app.models.users_roles_model import UserRole


def insert_into_users_roles_table():
    user_role_1 = UserRole.insert(
        id='cbe617a3-735a-4b46-922e-c771ecfe4498',
        user_id='9d5df0c7-2e78-46ef-bc66-6e4d01a421a4',
        role_id='5460c004-7ff5-438f-ab75-b388b8835620'
    ).execute()

    user_role_2 = UserRole.insert(
        id='91ffd72f-ac59-406e-93f4-7259155a1e78',
        user_id='289435a5-32af-4085-acf0-37ce1e248b36',
        role_id='5460c004-7ff5-438f-ab75-b388b8835620'
    ).execute()

    user_role_3 = UserRole.insert(
        id='dcbf3b18-66c3-40d8-8341-c9b89e4de571',
        user_id='380eb280-e0d3-4985-95f1-fb20d4cbc36f',
        role_id='5460c004-7ff5-438f-ab75-b388b8835620'
    ).execute()

    user_role_4 = UserRole.insert(
        id='14773c3f-36c5-4e70-beea-ead98b81e0ff',
        user_id='8c610701-0553-4f79-a120-5fe7789f9bb7',
        role_id='5460c004-7ff5-438f-ab75-b388b8835620'
    ).execute()

    user_role_5 = UserRole.insert(
        id='eac76e2b-80b9-4bb4-9dc7-d27589499bb0',
        user_id='656120b6-b91a-47bd-bf75-9dff3f53e095',
        role_id='5460c004-7ff5-438f-ab75-b388b8835620'
    ).execute()

    user_role_6 = UserRole.insert(
        id='7143b55a-890e-44a1-a37b-00e389261e12',
        user_id='642746f1-cad9-41b9-8514-b1e3c99a71a9',
        role_id='5460c004-7ff5-438f-ab75-b388b8835620'
    ).execute()

    user_role_7 = UserRole.insert(
        id='8098eeec-cd04-443b-9b9b-fb8d0ec7b231',
        user_id='977c73de-2e74-4a36-8bb6-c8a75b6df7b0',
        role_id='5460c004-7ff5-438f-ab75-b388b8835620'
    ).execute()

    user_role_8 = UserRole.insert(
        id='44bfd357-faa4-403d-b873-6474c504eece',
        user_id='9cd85463-be7f-446d-90f6-7dac41fd8168',
        role_id='5460c004-7ff5-438f-ab75-b388b8835620'
    ).execute()

    user_role_9 = UserRole.insert(
        id='d05d713e-6a48-4237-874d-bee0b6440f0a',
        user_id='b85ee741-e380-4180-99e2-8bf4ffef7205',
        role_id='5460c004-7ff5-438f-ab75-b388b8835620'
    ).execute()

    user_role_10 = UserRole.insert(
        id='8bd0a85e-3e0f-4a82-aca0-d3593e702b18',
        user_id='ccd0e881-4b61-4d97-af58-2ddd13211e4f',
        role_id='5460c004-7ff5-438f-ab75-b388b8835620'
    ).execute()

    user_role_11 = UserRole.insert(
        id='5a11492e-d892-4f49-9172-e5bd80cff57c',
        user_id='9d5df0c7-2e78-46ef-bc66-6e4d01a421a4',
        role_id='811d9cea-ddcd-4b49-9336-8ddc2e37d1d5'
    ).execute()

    user_role_12 = UserRole.insert(
        id='93f2cdf0-2d4d-4ae8-8418-aa30762e7c5d',
        user_id='289435a5-32af-4085-acf0-37ce1e248b36',
        role_id='811d9cea-ddcd-4b49-9336-8ddc2e37d1d5'
    ).execute()

    user_role_13 = UserRole.insert(
        id='7c1a375b-0f9c-4ce7-b353-06dddf727582',
        user_id='8c610701-0553-4f79-a120-5fe7789f9bb7',
        role_id='811d9cea-ddcd-4b49-9336-8ddc2e37d1d5'
    ).execute()

    user_role_14 = UserRole.insert(
        id='ae9b3724-5c0d-4b9c-9d5d-79c38a546819',
        user_id='656120b6-b91a-47bd-bf75-9dff3f53e095',
        role_id='811d9cea-ddcd-4b49-9336-8ddc2e37d1d5'
    ).execute()

    user_role_15 = UserRole.insert(
        id='c22b1fef-24d1-4cb1-9d10-83faed8b98ad',
        user_id='9cd85463-be7f-446d-90f6-7dac41fd8168',
        role_id='0789e447-1008-4efb-bf0e-fb9746061c34'
    ).execute()

    user_role_16 = UserRole.insert(
        id='5a7c363b-d216-4700-996a-832480ecadf6',
        user_id='ccd0e881-4b61-4d97-af58-2ddd13211e4f',
        role_id='0789e447-1008-4efb-bf0e-fb9746061c34'
    ).execute()


