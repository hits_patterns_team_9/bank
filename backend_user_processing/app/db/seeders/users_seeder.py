import hashlib
from backend_user_processing.app.models.users_model import User


def insert_users_into_table():
    user_1 = User.insert(
        id='9d5df0c7-2e78-46ef-bc66-6e4d01a421a4',
        username='Морозов Вадим Даниилович',
        email='pruframommijoi-2057@yopmail.com',
        phone='543(698)681-74-083',
        birth_date='1981-04-20',
        password='111'
    ).execute()

    user_2 = User.insert(
        id='289435a5-32af-4085-acf0-37ce1e248b36',
        username='Чернышева Полина Михайловна',
        email='nouzoneikottei-6976@yopmail.com',
        phone='663(598)232-69-913',
        birth_date='1982-07-18',
        password='111'
    ).execute()

    user_3 = User.insert(
        id='380eb280-e0d3-4985-95f1-fb20d4cbc36f',
        username='Попов Али Константинович',
        email='kaxeuxoinnonnei-3825@yopmail.com',
        phone='8(27)855-60-644',
        birth_date='1984-10-22',
        password='111'
    ).execute()

    user_4 = User.insert(
        id='8c610701-0553-4f79-a120-5fe7789f9bb7',
        username='Бессонов Иван Серафимович',
        email='tebaussojizu-8049@yopmail.com',
        phone='10(5564)970-67-080',
        birth_date='1987-10-02',
        password='111'
    ).execute()

    user_5 = User.insert(
        id='656120b6-b91a-47bd-bf75-9dff3f53e095',
        username='Прокофьева Надежда Арсентьевна',
        email='cropeugroigraxo-5560@yopmail.com',
        phone='71(455)110-86-427',
        birth_date='1988-11-23',
        password='111'
    ).execute()

    user_6 = User.insert(
        id='642746f1-cad9-41b9-8514-b1e3c99a71a9',
        username='Артамонов Артур Михайлович',
        email='cauddessuttaffoi-9711@yopmail.com',
        phone='93(67)659-32-023',
        birth_date='1990-04-02',
        password='111'
    ).execute()

    user_7 = User.insert(
        id='977c73de-2e74-4a36-8bb6-c8a75b6df7b0',
        username='Зуева Лидия Фёдоровна',
        email='loquittennaufe-4029@yopmail.com',
        phone='95(30)427-50-392',
        birth_date='1990-06-22',
        password='111'
    ).execute()

    user_8 = User.insert(
        id='9cd85463-be7f-446d-90f6-7dac41fd8168',
        username='Дорофеев Давид Макарович',
        email='cottizoprito-3894@yopmail.com',
        phone='4(44)236-83-014',
        birth_date='1990-11-01',
        password='111'
    ).execute()

    user_9 = User.insert(
        id='b85ee741-e380-4180-99e2-8bf4ffef7205',
        username='Попов Марк Саввич',
        email='loukogannatri-9627@yopmail.com',
        phone='5(1085)403-25-454',
        birth_date='1991-05-22',
        password='111'
    ).execute()

    user_10 = User.insert(
        id='ccd0e881-4b61-4d97-af58-2ddd13211e4f',
        username='Воронов Демид Кириллович',
        email='brasocriquello-6284@yopmail.com',
        phone='690(40)862-16-469',
        birth_date='1991-09-06',
        password='111'
    ).execute()
