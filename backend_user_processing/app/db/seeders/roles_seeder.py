from backend_user_processing.app.models.roles_model import Role


def insert_roles_into_table():
    role_1 = Role.insert(
        id='5460c004-7ff5-438f-ab75-b388b8835620',
        role_name='customer'
    ).execute()

    role_2 = Role.insert(
        id='811d9cea-ddcd-4b49-9336-8ddc2e37d1d5',
        role_name='manager'
    ).execute()

    role_3 = Role.insert(
        id='0789e447-1008-4efb-bf0e-fb9746061c34',
        role_name='admin'
    ).execute()
