from backend_user_processing.app.db.connect_db import db
from backend_user_processing.settings import USERS_DB_NAME

users_db_name = USERS_DB_NAME

if not db.table_exists(users_db_name):
    with db.connection_context():
        db.execute_sql(f"CREATE DATABASE {users_db_name} OWNER = postgres ENCODING 'UTF8' LC_COLLATE 'en_US.utf8' "
                       f"LC_CTYPE 'en_US.utf8';")
        print(f"База данных '{users_db_name}' успешно создана.")
else:
    print(f"База данных '{users_db_name}' уже существует.")

db.close()