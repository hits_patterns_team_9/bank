from peewee import PostgresqlDatabase
from playhouse.migrate import PostgresqlMigrator
from backend_user_processing.settings import *

db = PostgresqlDatabase(
    database=DB_NAME,
    user=DB_USER,
    password=DB_PASSWORD,
    host=DB_HOST,
    port=DB_PORT)

users_db = PostgresqlDatabase(
    database=USERS_DB_NAME,
    user=DB_USER,
    password=DB_PASSWORD,
    host=DB_HOST,
    port=DB_PORT)
