from backend_user_processing.app.db.seeders.users_seeder import insert_users_into_table
from backend_user_processing.app.db.seeders.roles_seeder import insert_roles_into_table
from backend_user_processing.app.db.seeders.users_roles_seeder import insert_into_users_roles_table


def insert_single_tables_data():
    insert_users_into_table()
    insert_roles_into_table()


def insert_relational_tables_data():
    insert_into_users_roles_table()


insert_single_tables_data()
insert_relational_tables_data()

