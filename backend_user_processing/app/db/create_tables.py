from backend_user_processing.app.models.users_model import User
from backend_user_processing.app.models.roles_model import Role
from backend_user_processing.app.models.users_roles_model import UserRole


def create_all_tables():
    User.create_table()
    Role.create_table()
    UserRole.create_table()


create_all_tables()
