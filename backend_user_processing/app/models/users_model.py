from peewee import *
from backend_user_processing.app.models.base_model import BaseModel


class User(BaseModel):
    username = TextField()
    email = TextField(unique=True)
    phone = TextField(unique=True)
    birth_date = DateField()
    password = TextField()

    class Meta:
        table_name = 'users'
