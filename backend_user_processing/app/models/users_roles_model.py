from peewee import *
from backend_user_processing.app.models.base_model import BaseModel
from backend_user_processing.app.models.users_model import User
from backend_user_processing.app.models.roles_model import Role


class UserRole(BaseModel):
    user_id = ForeignKeyField(User,
                              field='id',
                              constraint_name='fk_user_role_user',
                              on_delete='cascade')
    role_id = ForeignKeyField(Role,
                              field='id',
                              constraint_name='fk_user_role_role',
                              on_delete='cascade')

    class Meta:
        table_name = 'users_roles'
