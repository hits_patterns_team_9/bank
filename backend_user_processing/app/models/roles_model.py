from peewee import *
from backend_user_processing.app.models.base_model import BaseModel


class Role(BaseModel):
    role_name = TextField()

    class Meta:
        table_name = 'roles'
