from peewee import Model, UUIDField, SQL, DateTimeField
from backend_user_processing.app.db.connect_db import users_db


class BaseModel(Model):
    id = UUIDField(primary_key=True, unique=True,
                   constraints=[SQL('DEFAULT gen_random_uuid()')])
    created_at = DateTimeField(constraints=[SQL('DEFAULT CURRENT_TIMESTAMP')])
    updated_at = DateTimeField(constraints=[SQL('DEFAULT CURRENT_TIMESTAMP')])

    class Meta:
        database = users_db
