from flask import Blueprint, jsonify, request
from peewee import IntegrityError
from backend_user_processing.app.models.roles_model import Role
from backend_user_processing.app.models.users_model import User
from backend_user_processing.app.models.users_roles_model import UserRole
from backend_user_processing.app.utils.response_util import create_response
from backend_user_processing.settings import BACKEND_USER_PROCESSING_URL_PREFIX

users_bp = Blueprint('users', __name__, url_prefix=BACKEND_USER_PROCESSING_URL_PREFIX)


@users_bp.route('/users', methods=['GET'])
def get_users():
    query_users = User.select().dicts()
    if not query_users:
        response = create_response(False, 'Users not found')
        return jsonify(response), 404

    all_users = []
    for user in query_users:
        user_roles = Role.select().join(UserRole).where(UserRole.user_id == user['id']).dicts()
        roles = [role['role_name'] for role in user_roles]

        user_data = {
            'user_id': user['id'],
            'roles': roles,
            'username': user['username'],
            'email': user['email'],
            'phone': user['phone'],
            'birth_date': (user['birth_date']).strftime('%Y-%m-%d')
        }
        all_users.append(user_data)

    response = create_response(True, 'Users were found')
    return jsonify({'response': response,
                    'users': all_users})


@users_bp.route('/users/<uuid:id>', methods=['GET'])
def get_user_by_id(id):
    query_users = User.select().where(User.id == id).dicts()

    if not query_users:
        response = create_response(False, 'User not found')
        return jsonify(response), 404

    user = query_users[0]
    user_roles = Role.select().join(UserRole).where(UserRole.user_id == user['id']).dicts()
    roles = [role['role_name'] for role in user_roles]

    user_data = {
        'user_id': user['id'],
        'roles': roles,
        'username': user['username'],
        'email': user['email'],
        'phone': user['phone'],
        'birth_date': (user['birth_date']).strftime('%Y-%m-%d')
    }

    response = create_response(True, 'User was found')
    return jsonify({'response': response,
                    'users': user_data})


@users_bp.route('/users', methods=['POST'])
def create_user():
    data = request.get_json()
    username = data.get('username')
    email = data.get('email')
    phone = data.get('phone')
    birth_date = data.get('birth_date')
    password = data.get('password')

    existing_email = User.select().where(User.email == email).dicts()
    existing_phone = User.select().where(User.phone == phone).dicts()

    if existing_email and existing_phone:
        response = create_response(False, 'Phone or email already used, try another one!')
        return jsonify({'response': response}), 409

    if existing_email:
        response = create_response(False, 'Email already used, try another one!')
        return jsonify({'response': response}), 409

    if existing_phone:
        response = create_response(False, 'Phone already used, try another one!')
        return jsonify({'response': response}), 409

    User.insert(username=username,
                email=email,
                phone=phone,
                birth_date=birth_date,
                password=password,
                ).execute()

    query_new_user = User.select().where(User.email == email).dicts()
    user = query_new_user[0]

    customer_role_query = Role.select().where(Role.role_name == 'customer').dicts()
    customer_role = customer_role_query[0]

    UserRole.insert(
        user_id=user['id'],
        role_id=customer_role['id']
    ).execute()

    user_roles = Role.select().join(UserRole).where(UserRole.user_id == user['id']).dicts()
    roles = [role['role_name'] for role in user_roles]

    user_data = {
        'user_id': user['id'],
        'roles': roles,
        'username': user['username'],
        'email': user['email'],
        'phone': user['phone'],
        'birth_date': (user['birth_date']).strftime('%Y-%m-%d')
    }

    response = create_response(True, 'User was created')
    return jsonify({'response': response,
                    'created_user': user_data}), 201


@users_bp.route('/users/<uuid:id>', methods=['PUT'])
def update_user_info_by_id(id):
    data = request.get_json()
    username = data.get('username')
    email = data.get('email')
    phone = data.get('phone')
    birth_date = data.get('birth_date')

    existing_email = User.select().where(User.email == email).dicts()
    existing_phone = User.select().where(User.phone == phone).dicts()

    if existing_email and existing_phone:
        response = create_response(False, 'Phone or email already used, try another one!')
        return jsonify({'response': response}), 409

    if existing_email:
        response = create_response(False, 'Email already used, try another one!')
        return jsonify({'response': response}), 409

    if existing_phone:
        response = create_response(False, 'Phone already used, try another one!')
        return jsonify({'response': response}), 409

    try:
        User.update(username=username,
                    email=email,
                    phone=phone,
                    birth_date=birth_date).where(User.id == id).execute()
    except IntegrityError as e:
        response = create_response(False, 'Phone or email already used, try another one!')
        return jsonify({'response': response}), 409

    query_updated_user = User.select().where(User.id == id).dicts()
    user = query_updated_user[0]

    user_roles = Role.select().join(UserRole).where(UserRole.user_id == user['id']).dicts()
    roles = [role['role_name'] for role in user_roles]

    user_data = {
        'user_id': user['id'],
        'roles': roles,
        'username': user['username'],
        'email': user['email'],
        'phone': user['phone'],
        'birth_date': (user['birth_date']).strftime('%Y-%m-%d')
    }

    response = create_response(True, 'User was updated')
    return jsonify({'response': response,
                    'updated_user': user_data})


@users_bp.route('/users/<uuid:id>/roles', methods=['PATCH'])
def update_user_roles(id):
    data = request.get_json()
    request_roles = data.get('roles')
    user_roles = [role['role_name'] for role in Role.select().join(UserRole).where(UserRole.user_id == id).dicts()]

    for role in request_roles:
        role_query = Role.select().where(Role.role_name == role).dicts()
        if role in user_roles:
            UserRole.delete().where(UserRole.user_id == id, UserRole.role_id == role_query[0]['id']).execute()
        if role not in user_roles:
            UserRole.insert(
                user_id=id,
                role_id=role_query[0]['id']
            ).execute()

    query_updated_user = User.select().where(User.id == id).dicts()
    user = query_updated_user[0]
    user_roles = Role.select().join(UserRole).where(UserRole.user_id == user['id']).dicts()
    roles = [role['role_name'] for role in user_roles]

    user_data = {
        'user_id': user['id'],
        'roles': roles,
        'username': user['username'],
        'email': user['email'],
        'phone': user['phone'],
        'birth_date': user['birth_date'].strftime('%Y-%m-%d')
    }

    response = create_response(True, f'Roles were updated')
    return jsonify({'response': response, 'user': user_data})


@users_bp.route('/users/<uuid:id>', methods=['DELETE'])
def delete_user_by_id(id):
    query = User.select().where(User.id == id).dicts()
    if not query:
        response = create_response(False, 'User doesn\'t exist')
        return jsonify({'response': response}), 404

    User.delete().where(User.id == id).execute()
    response = create_response(True, 'User was deleted')
    return jsonify({'response': response})