from flask import Flask
from flask import request

from flask_cors import CORS
from backend_user_processing.app.controllers.users_controller import users_bp
from backend_user_processing.settings import *

import logging

app = Flask(__name__)
CORS(app)

app.logger.setLevel(logging.DEBUG)


@app.before_request
def log_request_info():
    app.logger.debug(f'\nHeaders: \n{request.headers}\nBody: \n{request.get_data().decode()}')


@app.route('/')
def hello():
    return 'User processing - backend app'


app.register_blueprint(users_bp)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=BACKEND_USER_PROCESSING_PORT)
