from flask import Flask
from flask import request

from flask_cors import CORS
from credit_issuer.app.controllers.credit_rules_controller import credits_rules_bp
from credit_issuer.settings import *

import logging

app = Flask(__name__)
CORS(app)

app.logger.setLevel(logging.DEBUG)


@app.before_request
def log_request_info():
    app.logger.debug(f'\nHeaders: \n{request.headers}\nBody: \n{request.get_data().decode()}')


@app.route('/')
def hello():
    return 'Credit issuer - backend app'


app.register_blueprint(credits_rules_bp)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=BACKEND_CREDIT_ISSUER_PORT)
