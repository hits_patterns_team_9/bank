import requests
from flask import Blueprint, jsonify, request
from credit_issuer.settings import *
from credit_issuer.app.rules.credit_rules import establish_credit_options, check_if_credit_sum_is_zero

credits_rules_bp = Blueprint('/credits', __name__, url_prefix=BACKEND_CREDIT_ISSUER_URL_PREFIX)


@credits_rules_bp.route('/credits', methods=['POST'])
def create_credit():
    data = request.get_json()
    user_id = data.get('user_id')
    credit_type = data.get('credit_type')
    credit_name = data.get('credit_name')
    money_type = data.get('money_type')
    expire_date = data.get('expire_date')

    credit_data = {
        'user_id': user_id,
        'credit_type': credit_type,
        'credit_name': credit_name,
        'money_type': money_type,
        'expire_date': expire_date
    }

    credit = establish_credit_options(credit_data)

    response = requests.post(
        f'http://{BACKEND_HOST}:{BACKEND_CORE_PORT}/{BACKEND_CORE_URL_PREFIX}/credits',
        json=credit)

    if response.status_code == 201:
        return jsonify(response.json()), 201
    else:
        return jsonify({'error': 'Error in the downstream request'}), 500


@credits_rules_bp.route('/credits/<string:credit_number>', methods=['DELETE'])
def delete_credit_by_credit_number(credit_number):
    response_credit = requests.get(
        f'http://{BACKEND_HOST}:{BACKEND_CORE_PORT}/{BACKEND_CORE_URL_PREFIX}/credits/{credit_number}')
    response_credit = response_credit.json()

    if response_credit['response']['status']:
        if response_credit['credit']['credit_sum'] == 0.0 or response_credit['credit']['credit_sum'] == 0:
            response = requests.delete(
                f'http://{BACKEND_HOST}:{BACKEND_CORE_PORT}/{BACKEND_CORE_URL_PREFIX}/credits/{credit_number}')
            if response.status_code == 200:
                return jsonify(response.json()), 200
    else:
        return jsonify({'error': 'Error in the downstream request'}), 500
