from datetime import datetime


def establish_credit_options(credit_data):
    user_id = credit_data['user_id']
    credit_type = credit_data['credit_type']
    credit_name = credit_data['credit_name']
    money_type = credit_data['money_type']
    expire_date = credit_data['expire_date']

    if credit_type == 'credit' and money_type == 'RUB':
        credit_rub = {'user_id': user_id,
                      'credit_name': credit_name,
                      'credit_type': credit_type,
                      'credit_sum': 0.0,
                      'money_type': money_type,
                      'service_price': 150,
                      'withdraw_low_limit_per_day': -150_000,
                      'withdraw_high_limit_per_day': 150_000,
                      'withdraw_low_limit_per_month': -1_500_000,
                      'withdraw_high_limit_per_month': 1_500_000,
                      'commission': 2.5,
                      'interest_rate': 10.5,
                      'created_date': f'{datetime.now()}',
                      'expire_date': expire_date,
                      'additional_conditions': '',
                      }
        return credit_rub

    if credit_type == 'debit' and money_type == 'RUB':
        debit_rub = {'user_id': user_id,
                     'credit_name': credit_name,
                     'credit_type': credit_type,
                     'credit_sum': 0.0,
                     'money_type': money_type,
                     'service_price': 70,
                     'withdraw_low_limit_per_day': 0,
                     'withdraw_high_limit_per_day': 150_000,
                     'withdraw_low_limit_per_month': 0,
                     'withdraw_high_limit_per_month': 1_500_000,
                     'commission': 1.5,
                     'interest_rate': 0.0,
                     'created_date': f'{datetime.now()}',
                     'expire_date': expire_date,
                     'additional_conditions': '',
                     }

        return debit_rub

    if credit_type == 'credit' and money_type == 'USD':
        credit_usd = {'user_id': user_id,
                      'credit_name': credit_name,
                      'credit_type': credit_type,
                      'credit_sum': 0.0,
                      'money_type': money_type,
                      'service_price': 70,
                      'withdraw_low_limit_per_day': -1000,
                      'withdraw_high_limit_per_day': 1000,
                      'withdraw_low_limit_per_month': -3000,
                      'withdraw_high_limit_per_month': 3000,
                      'commission': 2.5,
                      'interest_rate': 14.1,
                      'created_date': f'{datetime.now()}',
                      'expire_date': expire_date,
                      'additional_conditions': '',
                      }
        return credit_usd

    if credit_type == 'debit' and money_type == 'USD':
        debit_usd = {'user_id': user_id,
                     'credit_name': credit_name,
                     'credit_type': credit_type,
                     'credit_sum': 0.0,
                     'money_type': money_type,
                     'service_price': 2,
                     'withdraw_low_limit_per_day': 0,
                     'withdraw_high_limit_per_day': 1000,
                     'withdraw_low_limit_per_month': 0,
                     'withdraw_high_limit_per_month': 3000,
                     'commission': 2.5,
                     'interest_rate': 0.0,
                     'created_date': f'{datetime.now()}',
                     'expire_date': expire_date,
                     'additional_conditions': '',
                     }
        return debit_usd

    if credit_type == 'credit' and money_type == 'EURO':
        credit_usd = {'user_id': user_id,
                      'credit_name': credit_name,
                      'credit_type': credit_type,
                      'credit_sum': 0.0,
                      'money_type': money_type,
                      'service_price': 6,
                      'withdraw_low_limit_per_day': -1000,
                      'withdraw_high_limit_per_day': 1000,
                      'withdraw_low_limit_per_month': -3000,
                      'withdraw_high_limit_per_month': 3000,
                      'commission': 4.5,
                      'interest_rate': 16.7,
                      'created_date': f'{datetime.now()}',
                      'expire_date': expire_date,
                      'additional_conditions': '',
                      }
        return credit_usd

    if credit_type == 'debit' and money_type == 'USD':
        debit_usd = {'user_id': user_id,
                     'credit_name': credit_name,
                     'credit_type': credit_type,
                     'credit_sum': 0.0,
                     'money_type': money_type,
                     'service_price': 6,
                     'withdraw_low_limit_per_day': 0,
                     'withdraw_high_limit_per_day': 1000,
                     'withdraw_low_limit_per_month': 0,
                     'withdraw_high_limit_per_month': 3000,
                     'commission': 1.5,
                     'interest_rate': 0.0,
                     'created_date': f'{datetime.now()}',
                     'expire_date': expire_date,
                     'additional_conditions': '',
                     }
        return debit_usd


def check_if_credit_sum_is_zero(credit_sum):
    if credit_sum == 0 or credit_sum == 0.0:
        return True
    return False
