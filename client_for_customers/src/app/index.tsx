import { Layout } from "antd";
import {
  BrowserRouter,
  Navigate,
  Outlet,
  Route,
  Routes,
} from "react-router-dom";
import { CREDIT, CREDITS_LIST } from "../shared";
import { Suspense, lazy } from "react";

const CreditsList = lazy(() => import("../pages/CreditsList"));
const Credit = lazy(() => import("../pages/Credit"));
export const AppRoutes = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          element={
            <Layout>
              <Layout.Header></Layout.Header>
              <Layout.Content style={{ padding: "1rem" }}>
                <Suspense>
                  <Outlet />
                </Suspense>
              </Layout.Content>
            </Layout>
          }
        >
          <Route index path={CREDITS_LIST} element={<CreditsList />} />
          <Route path={CREDIT()} element={<Credit />} />
          <Route path="*" element={<Navigate to={CREDITS_LIST} />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};
