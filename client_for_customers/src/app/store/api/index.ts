import { AppApi } from "../../../shared";

export const apiReducers = {
  [AppApi.reducerPath]: AppApi.reducer,
};

export const apiMiddleware = [AppApi.middleware];
