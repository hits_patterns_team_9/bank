import { injectToApi } from "../../../../shared";

export type CreateCreditRequest = {
  credit_name: string;
  credit_sum: number;
  credit_type: string;
  money_type: string;
  service_price: number;
  withdraw_low_limit_per_day: number;
  withdraw_high_limit_per_day: number;
  withdraw_low_limit_per_month: number;
  withdraw_high_limit_per_month: number;
  commission: number;
  interest_rate: number;
  created_date: string;
  expire_date: string;
  additional_conditions: string;
  user_id: string;
};

export const createCreditApi = injectToApi({
  endpoints: (builder) => ({
    createCredit: builder.mutation<void, CreateCreditRequest>({
      query: (body) => ({
        url: `/credits`,
        method: "POST",
        body,
      }),
      invalidatesTags: ["GET_CREDITS_LIST"],
    }),
  }),
});

export const { useCreateCreditMutation } = createCreditApi;
