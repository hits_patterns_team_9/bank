import { Button, Form, Input, Modal } from "antd";

import { CreateCreditRequest, useCreateCreditMutation } from "./api";
import { useState } from "react";

type CreateCreditProps = { user_id: string };

type FormBody = Pick<
  Omit<CreateCreditRequest, "user_id">,
  "credit_name" | "credit_type" | "money_type"
>;
export const CreateCredit = ({ user_id }: CreateCreditProps) => {
  const [createCredit] = useCreateCreditMutation();

  const [open, setOpen] = useState<boolean>(false);
  const createCreditHandler = (body: FormBody) => {
    createCredit({
      ...body,
      user_id,
      credit_sum: 0.0,
      service_price: 70,
      withdraw_low_limit_per_day: 0,
      withdraw_high_limit_per_day: 0,
      withdraw_low_limit_per_month: 0,
      withdraw_high_limit_per_month: 0,
      commission: 0.1,
      interest_rate: 0.1,
      created_date: "2024-03-08 19:09:34",
      expire_date: "2028-03-08 19:09:34",
      additional_conditions: "какие то мутные условия",
    })
      .unwrap()
      .then(() => {
        setOpen(false);
      });
  };

  return (
    <>
      <Modal
        title={"Создать счёт"}
        open={open}
        onCancel={() => {
          setOpen(false);
        }}
        footer={null}
      >
        <Form<FormBody> onFinish={createCreditHandler} layout="vertical">
          <Form.Item<FormBody>
            name="credit_name"
            label="credit_name"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<FormBody>
            name="credit_type"
            label="credit_type"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<FormBody>
            name="money_type"
            label="money_type"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>

          <Form.Item>
            <Button htmlType="submit">Создать</Button>
          </Form.Item>
        </Form>
      </Modal>
      <Button
        onClick={() => {
          setOpen(true);
        }}
      >
        создать счёт
      </Button>
    </>
  );
};
