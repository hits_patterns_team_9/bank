import { Button, Form, InputNumber, Modal, Radio } from "antd";
import { useState } from "react";
import { useDepositMutation, useWithdrawMutation } from "./api";

interface DepositOrWithdrawModalProps {
  credit_number: string;
}
export const DepositOrWithdrawModal = ({
  credit_number,
}: DepositOrWithdrawModalProps) => {
  const [open, setOpen] = useState<boolean>(false);

  const [deposit] = useDepositMutation();
  const [withdraw] = useWithdrawMutation();

  const onFinish = ({
    credit_sum,
    type,
  }: {
    credit_sum: string;
    type: string;
  }) => {
    if (type === "deposit") {
      deposit({ credit_number, credit_sum });
    } else {
      withdraw({ credit_number, credit_sum });
    }
  };

  const [form] = Form.useForm();
  const type = Form.useWatch("type", form);
  console.log(type);
  return (
    <>
      <Modal open={open} onCancel={() => setOpen(false)} footer={null}>
        <Form
          onFinish={onFinish}
          form={form}
          initialValues={{ type: "deposit" }}
        >
          <Form.Item label="Сумма" name={"credit_sum"}>
            <InputNumber />
          </Form.Item>
          <Form.Item name={"type"}>
            <Radio.Group>
              <Radio value={"deposit"}>Пополнить</Radio>
              <Radio value={"withdraw"}>Снять</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item>
            <Button htmlType="submit">
              {type === "deposit" ? "Пополнить" : "Снять"}
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      <Button onClick={() => setOpen(true)}>Пополнить или снять деньги</Button>
    </>
  );
};
