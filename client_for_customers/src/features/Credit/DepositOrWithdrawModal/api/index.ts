import { injectToApi } from "../../../../shared";

export const depositOrWithdrawApi = injectToApi({
  endpoints: (builder) => ({
    deposit: builder.mutation<
      void,
      { credit_number: string; credit_sum: string }
    >({
      query: ({ credit_number, credit_sum }) => ({
        url: `/credits/${credit_number}/deposit`,
        method: "PATCH",
        body: {
          credit_sum,
        },
      }),
      invalidatesTags: ["GET_CREDIT_HISTORY", "GET_CREDIT_INFO"],
    }),
    withdraw: builder.mutation<
      void,
      { credit_number: string; credit_sum: string }
    >({
      query: ({ credit_number, credit_sum }) => ({
        url: `/credits/${credit_number}/withdraw`,
        method: "PATCH",
        body: {
          credit_sum,
        },
      }),
      invalidatesTags: ["GET_CREDIT_HISTORY", "GET_CREDIT_INFO"],
    }),
  }),
});

export const { useDepositMutation, useWithdrawMutation } = depositOrWithdrawApi;
