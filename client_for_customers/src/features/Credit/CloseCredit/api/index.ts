import { injectToApi } from "../../../../shared";

export const closeCreditApi = injectToApi({
  endpoints: (builder) => ({
    closeCredit: builder.mutation<void, { credit_number: string }>({
      query: ({ credit_number }) => ({
        url: `/credits/${credit_number}`,
        method: "DELETE",
      }),
      invalidatesTags: ["GET_CREDITS_LIST"],
    }),
  }),
});

export const { useCloseCreditMutation } = closeCreditApi;
