import { Button, ButtonProps } from "antd";
import { useCloseCreditMutation } from "./api";
import { useNavigate } from "react-router-dom";
import { CREDITS_LIST } from "../../../shared";

type CloseCreditProps = { credit_number: string } & Omit<
  ButtonProps,
  "onClick"
>;
export const CloseCreditButton = ({
  credit_number,
  ...props
}: CloseCreditProps) => {
  const [closeCredit] = useCloseCreditMutation();
  const navigate = useNavigate();
  const closeCreditHandler = () => {
    closeCredit({ credit_number })
      .unwrap()
      .then(() => {
        navigate(CREDITS_LIST);
      });
  };
  return <Button {...props} onClick={closeCreditHandler} />;
};
