import { Flex, Typography } from "antd";
import { CREDIT, useCreditsListQuery } from "../shared";
import { CreditistItem } from "../entities";
import { Link } from "react-router-dom";
import { CreateCredit } from "../features/Credit/CreateCredit";

const CreditsList = () => {
  const { data } = useCreditsListQuery({
    user_id: import.meta.env.VITE_USER_ID,
  });

  return (
    <>
      <Flex justify="space-between" align="center">
        <Typography.Title style={{ margin: 0 }}>Счета</Typography.Title>
        <CreateCredit user_id={import.meta.env.VITE_USER_ID} />
      </Flex>

      <Flex gap={16} vertical>
        {data?.user_credits?.map((credit) => {
          return (
            <Link key={credit.credit_id} to={CREDIT(credit.credit_number)}>
              <CreditistItem credit={credit} />
            </Link>
          );
        })}
      </Flex>
    </>
  );
};

export default CreditsList;
