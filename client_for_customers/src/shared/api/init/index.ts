import { EndpointDefinitions, createApi } from "@reduxjs/toolkit/query/react";
import { fetchBaseQueryRefreshToken } from "../queries/baseQuery";

export const AppApi = createApi({
  reducerPath: "AppApi",
  baseQuery: fetchBaseQueryRefreshToken,
  endpoints: () => ({}),
  tagTypes: ["GET_CREDITS_LIST", "GET_CREDIT_INFO", "GET_CREDIT_HISTORY"],
});

export const injectToApi = <T extends EndpointDefinitions>(
  injection: Parameters<typeof AppApi.injectEndpoints<T>>[0]
) => {
  return AppApi.injectEndpoints<T>(injection);
};
