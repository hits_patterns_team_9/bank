import { Card, Flex, Typography } from "antd";
import { Credit } from "../../../shared";

const { Text } = Typography;
interface ListItemProps {
  credit: Credit;
}
export const CreditistItem = ({
  credit: { credit_name, credit_sum, credit_type, credit_number, money_type },
}: ListItemProps) => {
  return (
    <Card>
      <Flex vertical>
        <Text>Имя: {credit_name}</Text>
        <Text>Номер: {credit_number}</Text>
        <Text>Сумма: {credit_sum}</Text>
        <Text>Тип: {credit_type}</Text>
        <Text>Валюта: {money_type}</Text>
      </Flex>
    </Card>
  );
};
