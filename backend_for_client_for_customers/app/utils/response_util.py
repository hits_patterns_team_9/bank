
def create_response(status, message):
    response = {
        "status": status,
        "message": message,
    }
    return response
