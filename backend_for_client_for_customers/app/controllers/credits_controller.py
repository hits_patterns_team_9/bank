import requests
from flask import Blueprint, jsonify, request
from backend_for_client_for_customers.settings import *

credits_customers_bp = Blueprint('/credits', __name__, url_prefix=BACKEND_FOR_CUSTOMERS_URL_PREFIX)


@credits_customers_bp.route('/credits', methods=['POST'])
def create_credit_customers():
    data = request.get_json()
    user_id = data.get('user_id')
    credit_type = data.get('credit_type')
    credit_name = data.get('credit_name')
    money_type = data.get('money_type')
    expire_date = data.get('expire_date')

    request_credit = {
        'user_id': user_id,
        'credit_type': credit_type,
        'credit_name': credit_name,
        'money_type': money_type,
        'expire_date': expire_date
    }

    response = requests.post(
        f'http://{BACKEND_HOST}:{BACKEND_CREDIT_ISSUER_PORT}/{BACKEND_CREDIT_ISSUER_URL_PREFIX}/credits',
        json=request_credit)
    if response.status_code == 201:
        return jsonify(response.json()), 201
    else:
        return jsonify({'error': 'Error in the downstream request'}), 500


@credits_customers_bp.route('/credits/<string:credit_number>', methods=['DELETE'])
def close_credit_customers(credit_number):
    url = f'http://{BACKEND_HOST}:{BACKEND_CREDIT_ISSUER_PORT}/{BACKEND_CREDIT_ISSUER_URL_PREFIX}/credits/{credit_number}'
    response = requests.delete(url)
    if response.status_code == 200:
        return jsonify(response.json()), 200
    else:
        return jsonify({'error': 'Error in the downstream request'}), 500


@credits_customers_bp.route('/credits', methods=['GET'])
def get_all_credits_customers():
    response = requests.get(
        f'http://{BACKEND_HOST}:{BACKEND_CORE_PORT}/{BACKEND_CORE_URL_PREFIX}/credits')

    if response.status_code == 200:
        return jsonify(response.json()), 200
    else:
        return jsonify({'error': 'Error in the downstream request'}), 500


@credits_customers_bp.route('/credits/users/<uuid:user_id>', methods=['GET'])
def get_user_credits_customers(user_id):
    response = requests.get(
        f'http://{BACKEND_HOST}:{BACKEND_CORE_PORT}/{BACKEND_CORE_URL_PREFIX}/credits/users/{user_id}')

    if response.status_code == 200:
        return jsonify(response.json()), 200
    else:
        return jsonify({'error': 'Error in the downstream request'}), 500


@credits_customers_bp.route('/credits/<string:credit_number>', methods=['GET'])
def get_credit_by_credit_number_customers(credit_number):
    response = requests.get(
        f'http://{BACKEND_HOST}:{BACKEND_CORE_PORT}/{BACKEND_CORE_URL_PREFIX}/credits/{credit_number}')

    if response.status_code == 200:
        return jsonify(response.json()), 200
    else:
        return jsonify({'error': 'Error in the downstream request'}), 500


@credits_customers_bp.route('/credits/<string:credit_number>/history', methods=['GET'])
def get_credit_history_customers(credit_number):
    response = requests.get(
        f'http://{BACKEND_HOST}:{BACKEND_CORE_PORT}/{BACKEND_CORE_URL_PREFIX}/credits/{credit_number}/history')

    if response.status_code == 200:
        return jsonify(response.json()), 200
    else:
        return jsonify({'error': 'Error in the downstream request'}), 500


@credits_customers_bp.route('/credits/<string:credit_number>/deposit', methods=['PATCH'])
def deposit_money_on_credit_customers(credit_number):
    data = request.get_json()
    credit_sum = data.get('credit_sum')

    request_credit = {
        'credit_sum': credit_sum
    }

    response = requests.patch(
        f'http://{BACKEND_HOST}:{BACKEND_CORE_PORT}/{BACKEND_CORE_URL_PREFIX}/credits/{credit_number}/deposit',
        json=request_credit)

    if response.status_code == 200:
        return jsonify(response.json()), 200
    else:
        return jsonify({'error': 'Error in the downstream request'}), 500


@credits_customers_bp.route('/credits/<string:credit_number>/withdraw', methods=['PATCH'])
def withdraw_money_on_credit_customers(credit_number):
    data = request.get_json()
    credit_sum = data.get('credit_sum')

    request_credit = {
        'credit_sum': credit_sum
    }

    response = requests.patch(
        f'http://{BACKEND_HOST}:{BACKEND_CORE_PORT}/{BACKEND_CORE_URL_PREFIX}/credits/{credit_number}/withdraw',
        json=request_credit)

    if response.status_code == 200:
        return jsonify(response.json()), 200
    else:
        return jsonify({'error': 'Error in the downstream request'}), 500


'''
/credits/users/{user_id}
/credits/{credit_number}
/credits/{credit_number}/history
/credits/{credit_number}/deposit
/credits/{credit_number}/withdraw
/credits
'''
