import os
from dotenv import load_dotenv
from pathlib import Path

load_dotenv()
env_path = Path('./core') / '.env'
load_dotenv(dotenv_path=env_path)

DB_NAME = os.getenv("DB_NAME")
USERS_DB_NAME = os.getenv("USERS_DB_NAME")
DB_USER = os.getenv("DB_USER")
DB_PASSWORD = os.getenv("DB_PASSWORD")
DB_HOST = os.getenv("DB_HOST")
DB_PORT = os.getenv("DB_PORT")

BACKEND_HOST = os.getenv("BACKEND_HOST")
BACKEND_CORE_HOST = os.getenv("BACKEND_CORE_HOST")

BACKEND_CORE_PORT = os.getenv("BACKEND_CORE_PORT")
BACKEND_USER_PROCESSING_PORT = os.getenv("BACKEND_USER_PROCESSING_PORT")
BACKEND_FOR_CUSTOMERS_PORT = os.getenv("BACKEND_FOR_CUSTOMERS_PORT")
BACKEND_FOR_MANAGERS_PORT = os.getenv("BACKEND_FOR_MANAGERS_PORT")
BACKEND_CREDIT_ISSUER_PORT = os.getenv("BACKEND_CREDIT_ISSUER_PORT")

BACKEND_CORE_URL_PREFIX = os.getenv("BACKEND_CORE_URL_PREFIX")
BACKEND_USER_PROCESSING_URL_PREFIX = os.getenv("BACKEND_USER_PROCESSING_URL_PREFIX")
BACKEND_FOR_CUSTOMERS_URL_PREFIX = os.getenv("BACKEND_FOR_CUSTOMERS_URL_PREFIX")
BACKEND_FOR_MANAGERS_URL_PREFIX = os.getenv("BACKEND_FOR_MANAGERS_URL_PREFIX")
BACKEND_CREDIT_ISSUER_URL_PREFIX = os.getenv("BACKEND_CREDIT_ISSUER_URL_PREFIX")
