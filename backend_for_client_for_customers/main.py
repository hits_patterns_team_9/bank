from flask import Flask
from flask import request

from flask_cors import CORS

from backend_for_client_for_customers.app.controllers.credits_controller import credits_customers_bp
from backend_for_client_for_customers.settings import *
import logging

app = Flask(__name__)
CORS(app)

app.logger.setLevel(logging.DEBUG)


@app.before_request
def log_request_info():
    app.logger.debug(f'\nHeaders: \n{request.headers}\nBody: \n{request.get_data().decode()}')


@app.route('/')
def hello():
    return 'Backend for customer - backend app'


app.register_blueprint(credits_customers_bp)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8002)
