# bank

## Frontend client_for_customers

Находясь в директории /client_for_customers

docker build -t client_for_customers .  
docker run --name client_for_customers -p 6001:6001 client_for_customers

Проект должен быть доступен по localhost:6001

### Database. Создание таблиц и генерация данных

#### Операции в директории /core.

Для выполения команд по созданию таблиц и данных для них необходимо выполнить

```sh
pipenv shell
```
Создание базы данных

```sh
pipenv createdb
```

Удаление всех таблиц:

```sh
pipenv dropdb
```

Пересоздание всех таблиц:

```sh
pipenv recreate
```

Создание всех таблиц:

```sh
pipenv initdb
```

Заполнение всех таблиц тестовыми данными:

```sh
pipenv seed
```
