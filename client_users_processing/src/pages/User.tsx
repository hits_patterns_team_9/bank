import { Flex, Typography } from "antd";
import { useUserInfoQuery } from "../shared";
import { UsersListItem } from "../entities";
import { useParams } from "react-router-dom";
import { DeleteUserButton } from "../features/User/DeleteUser";
import { AddRole } from "../features/User/AddRole";

const User = () => {
  const { id } = useParams();
  const { data } = useUserInfoQuery({ user_id: id! });

  if (!data) {
    return null;
  }
  return (
    <>
      <Flex justify="space-between" align="center">
        <Typography.Title style={{ margin: 0 }}>Пользователь</Typography.Title>
        <Flex gap={16}>
          <AddRole user_id={id!} />
          <DeleteUserButton user_id={id!} danger>
            Удалить пользователя
          </DeleteUserButton>
        </Flex>
      </Flex>

      <Flex gap={16} vertical>
        <UsersListItem user={data.users!} />
      </Flex>
    </>
  );
};

export default User;
