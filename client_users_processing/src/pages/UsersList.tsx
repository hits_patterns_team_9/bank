import { Flex, Typography } from "antd";
import { USER, useUsersListQuery } from "../shared";
import { UsersListItem } from "../entities";
import { Link } from "react-router-dom";
import { CreateUser } from "../features/User/CreateUser";

const UsersList = () => {
  const { data } = useUsersListQuery();

  return (
    <>
      <Flex justify="space-between" align="center">
        <Typography.Title style={{ margin: 0 }}>Пользователи</Typography.Title>
        <CreateUser />
      </Flex>

      <Flex gap={16} vertical>
        {data?.users?.map((user) => {
          return (
            <Link key={user.user_id} to={USER(user.user_id)}>
              <UsersListItem user={user} />
            </Link>
          );
        })}
      </Flex>
    </>
  );
};

export default UsersList;
