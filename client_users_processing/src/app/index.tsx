import { Layout } from "antd";
import {
  BrowserRouter,
  Navigate,
  Outlet,
  Route,
  Routes,
} from "react-router-dom";
import { USER, USERS_LIST } from "../shared";
import { Suspense, lazy } from "react";

const UsersList = lazy(() => import("../pages/UsersList"));
const User = lazy(() => import("../pages/User"));

export const AppRoutes = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          element={
            <Layout>
              <Layout.Header></Layout.Header>
              <Layout.Content style={{ padding: "1rem" }}>
                <Suspense>
                  <Outlet />
                </Suspense>
              </Layout.Content>
            </Layout>
          }
        >
          <Route index path={USERS_LIST} element={<UsersList />} />
          <Route index path={USER()} element={<User />} />

          <Route path="*" element={<Navigate to={USERS_LIST} />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};
