import { Card, Flex, Typography } from "antd";
import { User } from "../../../shared";

const { Text } = Typography;
interface ListItemProps {
  user: User;
}
export const UsersListItem = ({
  user: { username, email, phone, birth_date, roles },
}: ListItemProps) => {
  return (
    <Card>
      <Flex vertical>
        <Text>Имя: {username}</Text>
        <Text>Email: {email}</Text>
        <Text>phone: {phone}</Text>
        <Text>birth_date: {birth_date}</Text>
        <Text>
          roles:
          {roles.map((val) => {
            return <div style={{ marginLeft: "16px" }}>{val}</div>;
          })}
        </Text>
      </Flex>
    </Card>
  );
};
