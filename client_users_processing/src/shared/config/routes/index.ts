export const USERS_LIST = "/users";
export const USER = (userId: string = ":id") => `/user/${userId}`;
