import { injectToApi } from "../..";
import { User } from "../../../lib/entities/User";

export const getBankAccountsApi = injectToApi({
  endpoints: (builder) => ({
    usersList: builder.query<{ users: Array<User> }, void>({
      query: () => ({
        url: `/users`,
        method: "GET",
      }),
      providesTags: ["GET_USERS_LIST"],
    }),
    userInfo: builder.query<{ users: User }, { user_id: string }>({
      query: ({ user_id }) => ({
        url: `/users/${user_id}`,
        method: "GET",
      }),
      providesTags: ["GET_USER_INFO"],
    }),
  }),
});

export const { useUsersListQuery, useUserInfoQuery } = getBankAccountsApi;
