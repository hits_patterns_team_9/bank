import { EndpointDefinitions, createApi } from "@reduxjs/toolkit/query/react";
import { fetchBaseQueryRefreshToken } from "../queries/baseQuery";

export const AppApi = createApi({
  reducerPath: "AppApi",
  baseQuery: fetchBaseQueryRefreshToken,
  endpoints: () => ({}),
  tagTypes: ["GET_USERS_LIST", "GET_USER_INFO"],
});

export const injectToApi = <T extends EndpointDefinitions>(
  injection: Parameters<typeof AppApi.injectEndpoints<T>>[0]
) => {
  return AppApi.injectEndpoints<T>(injection);
};
