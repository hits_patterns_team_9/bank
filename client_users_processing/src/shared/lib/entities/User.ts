export type User = {
  user_id: string;
  roles: Array<string>;
  username: string;
  email: string;
  phone: string;
  birth_date: string;
};
