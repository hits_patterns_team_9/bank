import { Button, Form, Input, Modal } from "antd";

import { useState } from "react";
import { UserBody, useCreateUserMutation } from "./api";

export const CreateUser = () => {
  const [createUser] = useCreateUserMutation();

  const [open, setOpen] = useState<boolean>(false);
  const createUserHandler = (body: UserBody) => {
    createUser(body)
      .unwrap()
      .then(() => {
        setOpen(false);
      });
  };

  return (
    <>
      <Modal
        title={"Создать счёт"}
        open={open}
        onCancel={() => {
          setOpen(false);
        }}
        footer={null}
      >
        <Form<UserBody> onFinish={createUserHandler} layout="vertical">
          <Form.Item<UserBody> name="username" label="ФИО">
            <Input />
          </Form.Item>
          <Form.Item<UserBody> name="email" label="email">
            <Input />
          </Form.Item>
          <Form.Item<UserBody> name="phone" label="phone">
            <Input />
          </Form.Item>
          <Form.Item<UserBody> name="birth_date" label="birth_date">
            <Input />
          </Form.Item>
          <Form.Item<UserBody> name="password" label="password">
            <Input.Password />
          </Form.Item>
          <Form.Item<UserBody> name="roles" label="roles">
            <Input />
          </Form.Item>
          <Form.Item>
            <Button htmlType="submit">Создать</Button>
          </Form.Item>
        </Form>
      </Modal>
      <Button
        onClick={() => {
          setOpen(true);
        }}
      >
        создать пользователя
      </Button>
    </>
  );
};
