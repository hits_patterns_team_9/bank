import { injectToApi } from "../../../../shared";

export type UserBody = {
  username: string;
  roles: string[];
  email: string;
  phone: string;
  birth_date: string;
  password: string;
};

export const createUserApi = injectToApi({
  endpoints: (builder) => ({
    createUser: builder.mutation<void, UserBody>({
      query: (body) => ({
        url: `/users`,
        method: "POST",
        body,
      }),
      invalidatesTags: ["GET_USERS_LIST"],
    }),
  }),
});

export const { useCreateUserMutation } = createUserApi;
