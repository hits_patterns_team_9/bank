import { injectToApi } from "../../../../shared";

export const deleteUserApi = injectToApi({
  endpoints: (builder) => ({
    deleteUser: builder.mutation<void, { user_id: string }>({
      query: ({ user_id }) => ({
        url: `/users/${user_id}`,
        method: "DELETE",
      }),
      invalidatesTags: ["GET_USERS_LIST"],
    }),
  }),
});

export const { useDeleteUserMutation } = deleteUserApi;
