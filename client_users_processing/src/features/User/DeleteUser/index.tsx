import { Button, ButtonProps } from "antd";
import { useNavigate } from "react-router-dom";
import { USERS_LIST } from "../../../shared";
import { useDeleteUserMutation } from "./api";

type DeleteUserProps = { user_id: string } & Omit<ButtonProps, "onClick">;
export const DeleteUserButton = ({ user_id, ...props }: DeleteUserProps) => {
  const [deleteUser] = useDeleteUserMutation();
  const navigate = useNavigate();
  const deleteUserHandler = () => {
    deleteUser({ user_id })
      .unwrap()
      .then(() => {
        navigate(USERS_LIST);
      });
  };
  return <Button {...props} onClick={deleteUserHandler} />;
};
