import { Button, Form, Input, Modal } from "antd";
import { useState } from "react";
import { useAddRolesMutation } from "./api";

interface AddRoleProps {
  user_id: string;
}
export const AddRole = ({ user_id }: AddRoleProps) => {
  const [open, setOpen] = useState<boolean>(false);

  const [addRole] = useAddRolesMutation();

  const onFinish = ({ roles }: { roles: string }) => {
    addRole({ user_id, roles: roles.split(",") });
  };

  return (
    <>
      <Modal open={open} onCancel={() => setOpen(false)} footer={null}>
        <Form onFinish={onFinish} layout="vertical">
          <Form.Item label="роли (через запятую)" name={"roles"}>
            <Input />
          </Form.Item>

          <Form.Item>
            <Button htmlType="submit">Добавить роли</Button>
          </Form.Item>
        </Form>
      </Modal>
      <Button onClick={() => setOpen(true)}>Добавить роли</Button>
    </>
  );
};
