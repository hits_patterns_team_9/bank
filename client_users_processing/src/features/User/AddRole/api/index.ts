import { injectToApi } from "../../../../shared";

export const addRolesApi = injectToApi({
  endpoints: (builder) => ({
    addRoles: builder.mutation<void, { user_id: string; roles: string[] }>({
      query: ({ user_id, roles }) => ({
        url: `/users/${user_id}/roles`,
        method: "PATCH",
        body: {
          roles,
        },
      }),
      invalidatesTags: ["GET_USER_INFO"],
    }),
  }),
});

export const { useAddRolesMutation } = addRolesApi;
