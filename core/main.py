from flask import Flask
from flask import request

from flask_cors import CORS

from core.app.controllers.credits_controller import credits_bp
from core.app.controllers.histories_controller import credit_history_bp, credits_histories_bp
from core.settings import *

import logging


app = Flask(__name__)
CORS(app)
app.logger.setLevel(logging.DEBUG)

@app.before_request
def log_request_info():
    app.logger.debug(f'\nHeaders: \n{request.headers}\nBody: \n{request.get_data().decode()}')


@app.route('/')
def hello():
    return 'Core - backend app'


app.register_blueprint(credits_bp)
app.register_blueprint(credit_history_bp)
app.register_blueprint(credits_histories_bp)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=BACKEND_CORE_PORT)
