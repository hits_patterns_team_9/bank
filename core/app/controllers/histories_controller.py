from flask import Blueprint, jsonify, request
from core.app.models.credits_model import Credit
from core.app.models.histories_model import History
from core.app.utils.response_util import create_response
from core.settings import BACKEND_CORE_URL_PREFIX

credit_history_bp = Blueprint('/credits/<string:credit_number>', __name__, url_prefix=BACKEND_CORE_URL_PREFIX)

credits_histories_bp = Blueprint('/credits/histories', __name__, url_prefix=BACKEND_CORE_URL_PREFIX)


@credit_history_bp.route('/credits/<string:credit_number>/history', methods=['GET'])
def get_credit_history(credit_number):
    query_row_credit = Credit.select().where(Credit.credit_number == credit_number).dicts()
    if not query_row_credit:
        response = create_response(False, 'Credit not found')
        return jsonify({'response': response}), 404

    credit = query_row_credit[0]
    query_row_history = History.select().where(History.credit_id == credit['id']).dicts()

    if not query_row_history:
        response = create_response(False, 'Credit\'s history is empty')
        return jsonify({'response': response})

    credit_history_result = [
        {
            'history_id': history['id'],
            'operation_name': history['operation_name'],
            'operation_type': history['operation_type'],
            'operation_sum': history['operation_sum'],
            'credit': {
                'credit_id': credit['id'],
                'user_id': credit['user_id'],
                'credit_number': credit['credit_number'],
                'credit_name': credit['credit_name'],
                'credit_type': credit['credit_type'],
                'credit_sum': credit['credit_sum'],
                'money_type': credit['money_type'],
                'service_price': credit['service_price'],
                'withdraw_low_limit_per_day': credit['withdraw_low_limit_per_day'],
                'withdraw_high_limit_per_day': credit['withdraw_high_limit_per_day'],
                'withdraw_low_limit_per_month': credit['withdraw_low_limit_per_month'],
                'withdraw_high_limit_per_month': credit['withdraw_high_limit_per_month'],
                'commission': credit['commission'],
                'interest_rate': credit['interest_rate'],
                'created_date': credit['created_date'],
                'expire_date': credit['expire_date'],
                'additional_conditions': credit['additional_conditions'],
            },
            'created_at': history['created_at'],
        } for history in query_row_history]

    response = create_response(True, 'Credit was found')

    return jsonify({'response': response,
                    'credit': credit_history_result})


@credits_histories_bp.route('/credits/history', methods=['GET'])
def get_all_histories():
    pass
