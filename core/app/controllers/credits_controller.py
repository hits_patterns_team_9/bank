import random

from flask import Blueprint, jsonify, request
from core.app.models.credits_model import Credit
from core.app.models.histories_model import History
from core.app.utils.response_util import create_response
from core.settings import BACKEND_CORE_URL_PREFIX

credits_bp = Blueprint('credits', __name__, url_prefix=BACKEND_CORE_URL_PREFIX)


@credits_bp.route('/credits', methods=['GET'])
def get_credits():
    query = Credit.select().dicts()
    if not query:
        response = create_response(False, 'Credits not found')
        return jsonify(response), 404

    all_credits = [
        {'credit_id': credit['id'],
         'user_id': credit['user_id'],
         'credit_number': credit['credit_number'],
         'credit_name': credit['credit_name'],
         'credit_type': credit['credit_type'],
         'credit_sum': credit['credit_sum'],
         'money_type': credit['money_type'],
         'service_price': credit['service_price'],
         'withdraw_low_limit_per_day': credit['withdraw_low_limit_per_day'],
         'withdraw_high_limit_per_day': credit['withdraw_high_limit_per_day'],
         'withdraw_low_limit_per_month': credit['withdraw_low_limit_per_month'],
         'withdraw_high_limit_per_month': credit['withdraw_high_limit_per_month'],
         'commission': credit['commission'],
         'interest_rate': credit['interest_rate'],
         'created_date': (credit['created_date']).strftime('%Y-%m-%d %H:%M:%S'),
         'expire_date': (credit['expire_date']).strftime('%Y-%m-%d %H:%M:%S'),
         'additional_conditions': credit['additional_conditions'],
         } for credit in query]

    response = create_response(True, 'Credits were found')
    return jsonify({'response': response,
                    'credits': all_credits})


@credits_bp.route('/credits/users/<uuid:id>', methods=['GET'])
def get_user_credits(id):
    query = Credit.select().where(Credit.user_id == id).dicts()
    if not query:
        response = create_response(False, 'Credits not found')
        return jsonify({'response': response}), 404

    all_credits = [
        {'credit_id': credit['id'],
         'user_id': credit['user_id'],
         'credit_number': credit['credit_number'],
         'credit_name': credit['credit_name'],
         'credit_type': credit['credit_type'],
         'credit_sum': credit['credit_sum'],
         'money_type': credit['money_type'],
         'service_price': credit['service_price'],
         'withdraw_low_limit_per_day': credit['withdraw_low_limit_per_day'],
         'withdraw_high_limit_per_day': credit['withdraw_high_limit_per_day'],
         'withdraw_low_limit_per_month': credit['withdraw_low_limit_per_month'],
         'withdraw_high_limit_per_month': credit['withdraw_high_limit_per_month'],
         'commission': credit['commission'],
         'interest_rate': credit['interest_rate'],
         'created_date': (credit['created_date']).strftime('%Y-%m-%d %H:%M:%S'),
         'expire_date': (credit['expire_date']).strftime('%Y-%m-%d %H:%M:%S'),
         'additional_conditions': credit['additional_conditions'],
         } for credit in query]

    response = create_response(True, 'User\'s credits were found')
    return jsonify({'response': response,
                    'user_credits': all_credits})


@credits_bp.route('/credits/<string:credit_number>', methods=['GET'])
def get_credit_by_credit_number(credit_number):
    query = Credit.select().where(Credit.credit_number == credit_number).dicts()
    if not query:
        response = create_response(False, 'Credit not found')
        return jsonify({'response': response}), 404

    credit = query[0]
    credit_result = {
        'credit_id': credit['id'],
        'user_id': credit['user_id'],
        'credit_number': credit['credit_number'],
        'credit_name': credit['credit_name'],
        'credit_type': credit['credit_type'],
        'credit_sum': credit['credit_sum'],
        'money_type': credit['money_type'],
        'service_price': credit['service_price'],
        'withdraw_low_limit_per_day': credit['withdraw_low_limit_per_day'],
        'withdraw_high_limit_per_day': credit['withdraw_high_limit_per_day'],
        'withdraw_low_limit_per_month': credit['withdraw_low_limit_per_month'],
        'withdraw_high_limit_per_month': credit['withdraw_high_limit_per_month'],
        'commission': credit['commission'],
        'interest_rate': credit['interest_rate'],
        'created_date': (credit['created_date']).strftime('%Y-%m-%d %H:%M:%S'),
        'expire_date': (credit['expire_date']).strftime('%Y-%m-%d %H:%M:%S'),
        'additional_conditions': credit['additional_conditions'],
    }
    response = create_response(True, 'Credit was found')

    return jsonify({'response': response,
                    'credit': credit_result})


@credits_bp.route('/credits/<string:credit_number>', methods=['DELETE'])
def delete_credit_by_credit_number(credit_number):
    query = Credit.select().where(Credit.credit_number == credit_number).dicts()
    if not query:
        response = create_response(False, 'Credit doesn\'t exist')
        return jsonify({'response': response}), 404

    Credit.delete().where(Credit.credit_number == credit_number).execute()
    response = create_response(True, 'Credit was deleted')
    return jsonify({'response': response})


@credits_bp.route('/credits/<string:credit_number>/deposit', methods=['PATCH'])
def deposit_money_on_credit(credit_number):
    data = request.get_json()
    sum_from_request = data.get('credit_sum')
    query = Credit.select().where(Credit.credit_number == credit_number).dicts()
    if not query:
        response = create_response(False, 'Credit doesn\'t exist')
        return jsonify({'response': response}), 404

    credit = query[0]
    credit_sum = float(sum_from_request) + float(credit['credit_sum'])

    Credit.update({Credit.credit_sum: credit_sum}).where(Credit.credit_number == credit_number).execute()
    response = create_response(True, 'Credit sum was updated')
    credit_result = {
        'credit_id': credit['id'],
        'user_id': credit['user_id'],
        'credit_number': credit['credit_number'],
        'credit_name': credit['credit_name'],
        'credit_type': credit['credit_type'],
        'credit_sum': credit['credit_sum'],
        'money_type': credit['money_type'],
        'service_price': credit['service_price'],
        'withdraw_low_limit_per_day': credit['withdraw_low_limit_per_day'],
        'withdraw_high_limit_per_day': credit['withdraw_high_limit_per_day'],
        'withdraw_low_limit_per_month': credit['withdraw_low_limit_per_month'],
        'withdraw_high_limit_per_month': credit['withdraw_high_limit_per_month'],
        'commission': credit['commission'],
        'interest_rate': credit['interest_rate'],
        'created_date': (credit['created_date']).strftime('%Y-%m-%d %H:%M:%S'),
        'expire_date': (credit['expire_date']).strftime('%Y-%m-%d %H:%M:%S'),
        'additional_conditions': credit['additional_conditions'],
    }

    credit_number = credit['credit_number']
    History.insert(operation_name=f'Пополнение баланса счета {credit_number}',
                   operation_type='deposit',
                   operation_sum=sum_from_request,
                   credit_id=credit['id']).execute()

    return jsonify({'response': response,
                    'updated_credit': credit_result})


@credits_bp.route('/credits/<string:credit_number>/withdraw', methods=['PATCH'])
def withdraw_money_from_credit(credit_number):
    data = request.get_json()
    sum_from_request = data.get('credit_sum')
    query = Credit.select().where(Credit.credit_number == credit_number).dicts()
    if not query:
        response = create_response(False, 'Credit doesn\'t exist')
        return jsonify({'response': response}), 404

    credit = query[0]
    credit_sum = float(credit['credit_sum']) - float(sum_from_request)

    Credit.update({Credit.credit_sum: credit_sum}).where(Credit.credit_number == credit_number).execute()
    response = create_response(True, 'Credit sum was updated')
    credit_result = {
        'credit_id': credit['id'],
        'user_id': credit['user_id'],
        'credit_number': credit['credit_number'],
        'credit_name': credit['credit_name'],
        'credit_type': credit['credit_type'],
        'credit_sum': credit['credit_sum'],
        'money_type': credit['money_type'],
        'service_price': credit['service_price'],
        'withdraw_low_limit_per_day': credit['withdraw_low_limit_per_day'],
        'withdraw_high_limit_per_day': credit['withdraw_high_limit_per_day'],
        'withdraw_low_limit_per_month': credit['withdraw_low_limit_per_month'],
        'withdraw_high_limit_per_month': credit['withdraw_high_limit_per_month'],
        'commission': credit['commission'],
        'interest_rate': credit['interest_rate'],
        'created_date': (credit['created_date']).strftime('%Y-%m-%d %H:%M:%S'),
        'expire_date': (credit['expire_date']).strftime('%Y-%m-%d %H:%M:%S'),
        'additional_conditions': credit['additional_conditions'],
    }
    # TODO: добавить запись в бд

    credit_number = credit['credit_number']
    History.insert(operation_name=f'Снятие денег со счета {credit_number}',
                   operation_type='withdraw',
                   operation_sum=sum_from_request,
                   credit_id=credit['id']).execute()

    return jsonify({'response': response,
                    'updated_credit': credit_result})


@credits_bp.route('/credits/<string:credit_number>', methods=['PUT'])
def change_credit_info(credit_number):
    data = request.get_json()
    credit_name = data.get('credit_name')
    credit_type = data.get('credit_type')
    money_type = data.get('money_type')
    expire_date = data.get('expire_date')

    query = Credit.select().where(Credit.credit_number == credit_number).dicts()
    if not query:
        response = create_response(False, 'Credit doesn\'t exist')
        return jsonify({'response': response}), 404

    credit = Credit.update(
        credit_name=credit_name,
        credit_type=credit_type,
        money_type=money_type,
        expire_date=expire_date
    ).where(Credit.credit_number == credit_number).execute()

    query_after_update = Credit.select().where(Credit.credit_number == credit_number).dicts()

    if query[0] == query_after_update[0]:
        response = create_response(False, 'No changes')
        return jsonify({'response': response, 'credit': credit})

    response = create_response(True, 'Credit was updated')
    credit = query_after_update[0]
    credit_result = {
        'credit_id': credit['id'],
        'user_id': credit['user_id'],
        'credit_number': credit['credit_number'],
        'credit_name': credit['credit_name'],
        'credit_type': credit['credit_type'],
        'credit_sum': credit['credit_sum'],
        'money_type': credit['money_type'],
        'service_price': credit['service_price'],
        'withdraw_low_limit_per_day': credit['withdraw_low_limit_per_day'],
        'withdraw_high_limit_per_day': credit['withdraw_high_limit_per_day'],
        'withdraw_low_limit_per_month': credit['withdraw_low_limit_per_month'],
        'withdraw_high_limit_per_month': credit['withdraw_high_limit_per_month'],
        'commission': credit['commission'],
        'interest_rate': credit['interest_rate'],
        'created_date': (credit['created_date']).strftime('%Y-%m-%d %H:%M:%S'),
        'expire_date': (credit['expire_date']).strftime('%Y-%m-%d %H:%M:%S'),
        'additional_conditions': credit['additional_conditions'],
    }
    return jsonify({'response': response, 'updated_credit': credit_result})


@credits_bp.route('/credits', methods=['POST'])
def create_credit():
    data = request.get_json()
    user_id = data.get('user_id')
    credit_type = data.get('credit_type')
    credit_name = data.get('credit_name')
    money_type = data.get('money_type')
    service_price = data.get('service_price')
    withdraw_low_limit_per_day = data.get('withdraw_low_limit_per_day')
    withdraw_high_limit_per_day = data.get('withdraw_high_limit_per_day')
    withdraw_low_limit_per_month = data.get('withdraw_low_limit_per_month')
    withdraw_high_limit_per_month = data.get('withdraw_high_limit_per_month')
    commission = data.get('commission')
    interest_rate = data.get('interest_rate')
    created_date = data.get('created_date')
    expire_date = data.get('expire_date')
    additional_conditions = data.get('additional_conditions')

    credit_number = generate_credit_number()
    existing_number = Credit.select().where(Credit.credit_number == credit_number).dicts()

    if existing_number:
        credit_number = generate_credit_number()

    Credit.insert(user_id=user_id,
                  credit_type=credit_type,
                  credit_number=credit_number,
                  credit_name=credit_name,
                  money_type=money_type,
                  service_price=service_price,
                  withdraw_low_limit_per_day=withdraw_low_limit_per_day,
                  withdraw_high_limit_per_day=withdraw_high_limit_per_day,
                  withdraw_low_limit_per_month=withdraw_low_limit_per_month,
                  withdraw_high_limit_per_month=withdraw_high_limit_per_month,
                  commission=commission,
                  interest_rate=interest_rate,
                  created_date=created_date,
                  expire_date=expire_date,
                  additional_conditions=additional_conditions).execute()

    query_new_credit = Credit.select().where(Credit.credit_number == credit_number).dicts()
    new_credit = query_new_credit[0]
    credit_result = {
        'credit_id': new_credit['id'],
        'user_id': new_credit['user_id'],
        'credit_number': new_credit['credit_number'],
        'credit_name': new_credit['credit_name'],
        'credit_type': new_credit['credit_type'],
        'credit_sum': new_credit['credit_sum'],
        'money_type': new_credit['money_type'],
        'service_price': new_credit['service_price'],
        'withdraw_low_limit_per_day': new_credit['withdraw_low_limit_per_day'],
        'withdraw_high_limit_per_day': new_credit['withdraw_high_limit_per_day'],
        'withdraw_low_limit_per_month': new_credit['withdraw_low_limit_per_month'],
        'withdraw_high_limit_per_month': new_credit['withdraw_high_limit_per_month'],
        'commission': new_credit['commission'],
        'interest_rate': new_credit['interest_rate'],
        'created_date': (new_credit['created_date']).strftime('%Y-%m-%d %H:%M:%S'),
        'expire_date': (new_credit['expire_date']).strftime('%Y-%m-%d %H:%M:%S'),
        'additional_conditions': new_credit['additional_conditions'],
    }
    response = create_response(True, 'Credit was created')

    return jsonify({'response': response,
                    'created_credit': credit_result}), 201


def generate_credit_number():
    credit_number = ""
    for _ in range(4):
        block = ''.join(random.choices('0123456789', k=4))  # Генерируем случайную строку из цифр длиной 4
        credit_number += f"{block}-"
    return credit_number.rstrip("-")
