from core.app.models.users_model import User
from core.app.models.credits_model import Credit
from core.app.models.histories_model import History


def drop_all_tables():
    User.delete().execute()
    User.drop_table(User, cascade=True)
    Credit.delete().execute()
    Credit.drop_table(Credit, cascade=True)
    History.delete().execute()
    History.drop_table(History, cascade=True)


drop_all_tables()
