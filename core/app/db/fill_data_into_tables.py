from core.app.db.seeders.users_table_seeder import insert_users_into_table
from core.app.db.seeders.credits_table_seeder import insert_credits_into_table
from core.app.db.seeders.histories_table_seeder import insert_histories_into_table


def insert_single_tables_data():
    insert_users_into_table()


def insert_relational_tables_data():
    insert_credits_into_table()
    insert_histories_into_table()


insert_single_tables_data()
insert_relational_tables_data()

