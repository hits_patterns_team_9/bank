from core.app.models.users_model import User
from datetime import datetime


def insert_users_into_table():
    user_1 = User.insert(id='0af2833d-a650-4743-ac88-1ad014b26f81',
                         user_id='9d5df0c7-2e78-46ef-bc66-6e4d01a421a4',
                         created_at=datetime.now(), updated_at=datetime.now()
                         ).execute()

    user_2 = User.insert(id='4e7f7d0e-49bd-43dd-8afc-8981e90e540e',
                         user_id='289435a5-32af-4085-acf0-37ce1e248b36',
                         created_at=datetime.now(), updated_at=datetime.now()
                         ).execute()

    user_3 = User.insert(id='bece96ee-88ea-4446-a799-58f19937f153',
                         user_id='380eb280-e0d3-4985-95f1-fb20d4cbc36f',
                         created_at=datetime.now(), updated_at=datetime.now()
                         ).execute()

    user_4 = User.insert(id='f4280590-1b49-4d82-ad5e-acb5195034d4',
                         user_id='8c610701-0553-4f79-a120-5fe7789f9bb7',
                         created_at=datetime.now(), updated_at=datetime.now()
                         ).execute()

    user_5 = User.insert(id='d40feb99-2a2d-446a-9f09-cb0240cffaad',
                         user_id='656120b6-b91a-47bd-bf75-9dff3f53e095',
                         created_at=datetime.now(), updated_at=datetime.now()
                         ).execute()

    user_6 = User.insert(id='3e85f560-a846-41ab-8e2b-68e0d62c6e27',
                         user_id='642746f1-cad9-41b9-8514-b1e3c99a71a9',
                         created_at=datetime.now(), updated_at=datetime.now()
                         ).execute()

    user_7 = User.insert(id='e41bb3dd-3c5b-4b2d-be31-5181f6db7035',
                         user_id='977c73de-2e74-4a36-8bb6-c8a75b6df7b0',
                         created_at=datetime.now(), updated_at=datetime.now()
                         ).execute()

    user_8 = User.insert(id='09630388-fa26-4674-a513-e5361203fb08',
                         user_id='9cd85463-be7f-446d-90f6-7dac41fd8168',
                         created_at=datetime.now(), updated_at=datetime.now()
                         ).execute()

    user_9 = User.insert(id='a9f6604d-d307-462f-8b7b-f7e49aa72b66',
                         user_id='b85ee741-e380-4180-99e2-8bf4ffef7205',
                         created_at=datetime.now(), updated_at=datetime.now()
                         ).execute()

    user_10 = User.insert(id='8c27e8fd-9718-485f-a379-eb5de532f15e',
                          user_id='ccd0e881-4b61-4d97-af58-2ddd13211e4f',
                          created_at=datetime.now(), updated_at=datetime.now()
                          ).execute()

# insert_users_into_table()