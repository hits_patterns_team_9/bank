from datetime import datetime, timedelta

from core.app.models.credits_model import Credit


def insert_credits_into_table():
    current_date = datetime.now()
    expire_date = datetime.now() + timedelta(weeks=365*4)
    credit_1 = Credit.insert(
        id='2e735b84-28c8-4dcb-a460-fd4139d4b2b5',
        user_id='0af2833d-a650-4743-ac88-1ad014b26f81',
        credit_number='0000-0000-0000-0001',
        credit_name='Бансковский счет №1',
        credit_sum=11120.12,
        credit_type='debit',
        money_type='EURO',
        service_price=70,
        withdraw_low_limit_per_day=0,
        withdraw_high_limit_per_day=150_000,
        withdraw_low_limit_per_month=0,
        withdraw_high_limit_per_month=1_500_000,
        commission=1.5,
        interest_rate=0.0,
        created_date=current_date,
        expire_date=expire_date,
        additional_conditions='название_условия_1 = шаблон_улосвия_1, название_условия_2 = шаблон_условия_2',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    credit_2 = Credit.insert(
        id='1e3441d6-cb19-4430-94f2-0c45e3d1c1fc',
        user_id='0af2833d-a650-4743-ac88-1ad014b26f81',
        credit_number='0000-0000-0000-0002',
        credit_name='Бансковский счет №2',
        credit_sum=-2000.0,
        credit_type='credit',
        money_type='RUB',
        service_price=150,
        withdraw_low_limit_per_day=-150_000,
        withdraw_high_limit_per_day=150_000,
        withdraw_low_limit_per_month=-1_500_000,
        withdraw_high_limit_per_month=1_500_000,
        commission=2.5,
        interest_rate=10.0,
        created_date=datetime.now(),
        expire_date=(datetime.now()+timedelta(days=365*4)),
        additional_conditions='''название_условия_1 = шаблон_улосвия_1, название_условия_2 = шаблон_условия_2 ''',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    credit_3 = Credit.insert(
        id='7d5c2f19-c285-45ea-868f-32fa21d6914f',
        user_id='4e7f7d0e-49bd-43dd-8afc-8981e90e540e',
        credit_number='0000-0000-0000-0003',
        credit_name='Бансковский счет №3',
        credit_sum=200.0,
        credit_type='credit',
        money_type='USD',
        service_price=3,
        withdraw_low_limit_per_day=-1000,
        withdraw_high_limit_per_day=1000,
        withdraw_low_limit_per_month=-5000,
        withdraw_high_limit_per_month=5000,
        commission=3.0,
        interest_rate=14.0,
        created_date=datetime.now(),
        expire_date=(datetime.now()+timedelta(days=365*4)),
        additional_conditions='''название_условия_1 = шаблон_улосвия_1, название_условия_2 = шаблон_условия_2 ''',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    credit_4 = Credit.insert(
        id='1b622802-0fec-481b-bec4-fa619ed20197',
        user_id='bece96ee-88ea-4446-a799-58f19937f153',
        credit_number='0000-0000-0000-0004',
        credit_name='Бансковский счет №4',
        credit_sum=-50000.0,
        credit_type='credit',
        money_type='RUB',
        service_price=150,
        withdraw_low_limit_per_day=-150_000,
        withdraw_high_limit_per_day=150_000,
        withdraw_low_limit_per_month=-1_500_000,
        withdraw_high_limit_per_month=1_500_000,
        commission=2.5,
        interest_rate=10.0,
        created_date=datetime.now(),
        expire_date=(datetime.now()+timedelta(days=365*4)),
        additional_conditions='''название_условия_1 = шаблон_улосвия_1, название_условия_2 = шаблон_условия_2 ''',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    credit_5 = Credit.insert(
        id='b3918cf4-21f5-4e9f-b27e-f6c0ea18dcb1',
        user_id='f4280590-1b49-4d82-ad5e-acb5195034d4',
        credit_number='0000-0000-0000-0005',
        credit_name='Бансковский счет №5',
        credit_sum=2000.0,
        credit_type='debit',
        money_type='USD',
        service_price=70,
        withdraw_low_limit_per_day=0,
        withdraw_high_limit_per_day=150_000,
        withdraw_low_limit_per_month=0,
        withdraw_high_limit_per_month=1_500_000,
        commission=1.5,
        interest_rate=0.0,
        created_date=datetime.now(),
        expire_date=(datetime.now()+timedelta(days=365*4)),
        additional_conditions='''название_условия_1 = шаблон_улосвия_1, название_условия_2 = шаблон_условия_2 ''',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    credit_6 = Credit.insert(
        id='dde72459-88eb-423f-8a31-d50704807247',
        user_id='d40feb99-2a2d-446a-9f09-cb0240cffaad',
        credit_number='0000-0000-0000-0006',
        credit_name='Бансковский счет №6',
        credit_sum=1000.12,
        credit_type='debit',
        money_type='RUB',
        service_price=70,
        withdraw_low_limit_per_day=0,
        withdraw_high_limit_per_day=150_000,
        withdraw_low_limit_per_month=0,
        withdraw_high_limit_per_month=1_500_000,
        commission=1.5,
        interest_rate=0.0,
        created_date=datetime.now(),
        expire_date=(datetime.now()+timedelta(days=365*4)),
        additional_conditions='''название_условия_1 = шаблон_улосвия_1, название_условия_2 = шаблон_условия_2 ''',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    credit_7 = Credit.insert(
        id='64a0a110-2131-4b84-842e-b6e4ee27c251',
        user_id='3e85f560-a846-41ab-8e2b-68e0d62c6e27',
        credit_number='0000-0000-0000-0007',
        credit_name='Бансковский счет №7',
        credit_sum=1000000.2,
        credit_type='debit',
        money_type='RUB',
        service_price=70,
        withdraw_low_limit_per_day=0,
        withdraw_high_limit_per_day=150_000,
        withdraw_low_limit_per_month=0,
        withdraw_high_limit_per_month=1_500_000,
        commission=1.5,
        interest_rate=0.0,
        created_date=datetime.now(),
        expire_date=(datetime.now()+timedelta(days=365*4)),
        additional_conditions='''название_условия_1 = шаблон_улосвия_1, название_условия_2 = шаблон_условия_2 ''',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    credit_8 = Credit.insert(
        id='0b8af097-6ae2-4fb0-a4fd-cfbd590f3967',
        user_id='e41bb3dd-3c5b-4b2d-be31-5181f6db7035',
        credit_number='0000-0000-0000-0008',
        credit_name='Бансковский счет №8',
        credit_sum=-500.18,
        credit_type='credit',
        money_type='EURO',
        service_price=3,
        withdraw_low_limit_per_day=-1000,
        withdraw_high_limit_per_day=1000,
        withdraw_low_limit_per_month=-5000,
        withdraw_high_limit_per_month=5000,
        commission=3.0,
        interest_rate=14.0,
        created_date=datetime.now(),
        expire_date=(datetime.now()+timedelta(days=365*4)),
        additional_conditions='''название_условия_1 = шаблон_улосвия_1, название_условия_2 = шаблон_условия_2''',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    credit_9 = Credit.insert(
        id='2b2dd504-e571-4234-b12a-1c3d68f26906',
        user_id='09630388-fa26-4674-a513-e5361203fb08',
        credit_number='0000-0000-0000-0009',
        credit_name='Бансковский счет №9',
        credit_sum=0.0,
        credit_type='debit',
        money_type='EURO',
        service_price=70,
        withdraw_low_limit_per_day=0,
        withdraw_high_limit_per_day=150_000,
        withdraw_low_limit_per_month=0,
        withdraw_high_limit_per_month=1_500_000,
        commission=1.5,
        interest_rate=0.0,
        created_date=datetime.now(),
        expire_date=(datetime.now()+timedelta(days=365*4)),
        additional_conditions='''название_условия_1 = шаблон_улосвия_1, название_условия_2 = шаблон_условия_2 ''',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    credit_10 = Credit.insert(
        id='3149511f-031a-4b8e-9253-8e52f4d51e47',
        user_id='a9f6604d-d307-462f-8b7b-f7e49aa72b66',
        credit_number='0000-0000-0000-0010',
        credit_name='Бансковский счет №10',
        credit_sum=12000.0,
        credit_type='debit',
        money_type='RUB',
        service_price=70,
        withdraw_low_limit_per_day=0,
        withdraw_high_limit_per_day=150_000,
        withdraw_low_limit_per_month=0,
        withdraw_high_limit_per_month=1_500_000,
        commission=1.5,
        interest_rate=0.0,
        created_date=datetime.now(),
        expire_date=(datetime.now()+timedelta(days=365*4)),
        additional_conditions='''название_условия_1 = шаблон_улосвия_1, название_условия_2 = шаблон_условия_2 ''',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    credit_11 = Credit.insert(
        id='d49c0c7a-f2a3-42fb-b688-b9efb39abb1b',
        user_id='8c27e8fd-9718-485f-a379-eb5de532f15e',
        credit_number='0000-0000-0000-0011',
        credit_name='Бансковский счет №11',
        credit_sum=1000.0,
        credit_type='credit',
        money_type='USD',
        service_price=3,
        withdraw_low_limit_per_day=-1000,
        withdraw_high_limit_per_day=1000,
        withdraw_low_limit_per_month=-5000,
        withdraw_high_limit_per_month=5000,
        commission=3.0,
        interest_rate=14.0,
        created_date=datetime.now(),
        expire_date=(datetime.now()+timedelta(days=365*4)),
        additional_conditions='''название_условия_1 = шаблон_улосвия_1, название_условия_2 = шаблон_условия_2 ''',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    credit_12 = Credit.insert(
        id='91b4f580-e755-423a-857c-f4cce0e3220e',
        user_id='8c27e8fd-9718-485f-a379-eb5de532f15e',
        credit_number='0000-0000-0000-0012',
        credit_name='Бансковский счет №12',
        credit_sum=1000.0,
        credit_type='debit',
        money_type='RUB',
        service_price=70,
        withdraw_low_limit_per_day=0,
        withdraw_high_limit_per_day=150_000,
        withdraw_low_limit_per_month=0,
        withdraw_high_limit_per_month=1_500_000,
        commission=1.5,
        interest_rate=0.0,
        created_date=datetime.now(),
        expire_date=(datetime.now()+timedelta(days=365*4)),
        additional_conditions='''название_условия_1 = шаблон_улосвия_1, название_условия_2 = шаблон_условия_2 ''',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    credit_13 = Credit.insert(
        id='eb4a19e9-a06c-4a9d-80e7-31e965cefeee',
        user_id='09630388-fa26-4674-a513-e5361203fb08',
        credit_number='0000-0000-0000-0013',
        credit_name='Бансковский счет №13',
        credit_sum=-100000.0,
        credit_type='credit',
        money_type='RUB',
        service_price=150,
        withdraw_low_limit_per_day=-150_000,
        withdraw_high_limit_per_day=150_000,
        withdraw_low_limit_per_month=-1_500_000,
        withdraw_high_limit_per_month=1_500_000,
        commission=2.5,
        interest_rate=10.0,
        created_date=datetime.now(),
        expire_date=(datetime.now()+timedelta(days=365*4)),
        additional_conditions='''название_условия_1 = шаблон_улосвия_1, название_условия_2 = шаблон_условия_2 ''',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

# insert_credits_into_table()
