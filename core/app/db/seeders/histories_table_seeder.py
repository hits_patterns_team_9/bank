from core.app.models.histories_model import History
from datetime import datetime


def insert_histories_into_table():
    history_1 = History.insert(
        id='2f9a04ed-3739-425e-8756-16f360b394f2',
        operation_name='Снятие средств',
        operation_type='withdraw',
        operation_sum=100.0,
        credit_id='1e3441d6-cb19-4430-94f2-0c45e3d1c1fc',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_2 = History.insert(
        id='e63ac2ea-5b46-4795-a5c0-7a04f53bce39',
        operation_name='Снятие средств',
        operation_type='withdraw',
        operation_sum=100.0,
        credit_id='2e735b84-28c8-4dcb-a460-fd4139d4b2b5',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_3 = History.insert(
        id='aae70654-8285-43dd-a0a3-f61cb1988fa3',
        operation_name='Снятие средств',
        operation_type='withdraw',
        operation_sum=20000.0,
        credit_id='2e735b84-28c8-4dcb-a460-fd4139d4b2b5',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_4 = History.insert(
        id='5074abf1-adb9-4948-8516-de741a3c416a',
        operation_name='Зачисление средств',
        operation_type='deposit',
        operation_sum=1000.0,
        credit_id='1b622802-0fec-481b-bec4-fa619ed20197',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_5 = History.insert(
        id='ebbdc932-ee7a-4aff-8412-49600562d39f',
        operation_name='Зачисление средств',
        operation_type='deposit',
        operation_sum=400.0,
        credit_id='b3918cf4-21f5-4e9f-b27e-f6c0ea18dcb1',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_6 = History.insert(
        id='6f09db43-1e28-4ae2-beed-cfa448627161',
        operation_name='Зачисление средств',
        operation_type='deposit',
        operation_sum=10000.0,
        credit_id='dde72459-88eb-423f-8a31-d50704807247',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_7 = History.insert(
        id='189fef20-ecf1-4c44-b2ff-068a097cae7b',
        operation_name='Зачисление средств',
        operation_type='deposit',
        operation_sum=2200.0,
        credit_id='64a0a110-2131-4b84-842e-b6e4ee27c251',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_8 = History.insert(
        id='c256376e-5802-4422-8505-f8715dc9fa79',
        operation_name='Снятие средств',
        operation_type='withdraw',
        operation_sum=100.0,
        credit_id='0b8af097-6ae2-4fb0-a4fd-cfbd590f3967',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_9 = History.insert(
        id='3c95c9b4-f749-4fc3-9d15-762a03bf650a',
        operation_name='Снятие средств',
        operation_type='withdraw',
        operation_sum=100.0,
        credit_id='0b8af097-6ae2-4fb0-a4fd-cfbd590f3967',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_10 = History.insert(
        id='c17043da-79d9-4ecb-8df9-2b1695774bfb',
        operation_name='Снятие средств',
        operation_type='withdraw',
        operation_sum=102343.0,
        credit_id='2b2dd504-e571-4234-b12a-1c3d68f26906',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_11 = History.insert(
        id='3f5e865a-9b66-4d1b-acfd-e6e516af3674',
        operation_name='Снятие средств',
        operation_type='withdraw',
        operation_sum=100.0,
        credit_id='2b2dd504-e571-4234-b12a-1c3d68f26906',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_12 = History.insert(
        id='7ef5a4b3-ae96-4ad4-9db4-5c15d180fc3c',
        operation_name='Снятие средств',
        operation_type='withdraw',
        operation_sum=10234230.0,
        credit_id='91b4f580-e755-423a-857c-f4cce0e3220e',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_13 = History.insert(
        id='73bd5f55-e045-4e9a-8d4f-d6eeb0bea0a5',
        operation_name='Зачисление средств',
        operation_type='deposit',
        operation_sum=102340.0,
        credit_id='d49c0c7a-f2a3-42fb-b688-b9efb39abb1b',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_14 = History.insert(
        id='89b7532c-43eb-4430-b7e2-f3928415585d',
        operation_name='Зачисление средств',
        operation_type='deposit',
        operation_sum=100.0,
        credit_id='0b8af097-6ae2-4fb0-a4fd-cfbd590f3967',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_15 = History.insert(
        id='2b59aa5c-9b0c-46ee-a914-c83be3f465e0',
        operation_name='Зачисление средств',
        operation_type='deposit',
        operation_sum=23100.0,
        credit_id='3149511f-031a-4b8e-9253-8e52f4d51e47',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_16 = History.insert(
        id='47dcef0a-c436-4fb7-8314-5c60905b2d86',
        operation_name='Зачисление средств',
        operation_type='deposit',
        operation_sum=100.0,
        credit_id='0b8af097-6ae2-4fb0-a4fd-cfbd590f3967',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_17 = History.insert(
        id='9d203e69-8c25-4f69-9624-0d9355aaed29',
        operation_name='Зачисление средств',
        operation_type='deposit',
        operation_sum=100.0,
        credit_id='3149511f-031a-4b8e-9253-8e52f4d51e47',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_18 = History.insert(
        id='cadb3fd8-aea8-4e2d-b1b4-08c38e2b69e2',
        operation_name='Зачисление средств',
        operation_type='deposit',
        operation_sum=12300.0,
        credit_id='3149511f-031a-4b8e-9253-8e52f4d51e47',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_19 = History.insert(
        id='64c736c6-bad7-4c5d-a53a-ad9ce92c312d',
        operation_name='Снятие средств',
        operation_type='withdraw',
        operation_sum=1300.0,
        credit_id='2b2dd504-e571-4234-b12a-1c3d68f26906',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

    history_20 = History.insert(
        id='0ade585c-7b6e-499c-a35b-5fbb5981d465',
        operation_name='Снятие средств',
        operation_type='withdraw',
        operation_sum=1003.0,
        credit_id='2b2dd504-e571-4234-b12a-1c3d68f26906',
        created_at=datetime.now(),
        updated_at=datetime.now()
    ).execute()

# insert_histories_into_table()