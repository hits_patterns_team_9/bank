from core.app.models.users_model import User
from core.app.models.credits_model import Credit
from core.app.models.histories_model import History


def create_all_tables():
    User.create_table()
    Credit.create_table()
    History.create_table()


create_all_tables()
