from peewee import PostgresqlDatabase
from playhouse.migrate import PostgresqlMigrator
from core.settings import *

db = PostgresqlDatabase(
    database=DB_NAME,
    user=DB_USER,
    password=DB_PASSWORD,
    host=DB_HOST,
    port=DB_PORT)

bank_data_db = PostgresqlDatabase(
    database=BANK_DATA_DB_NAME,
    user=DB_USER,
    password=DB_PASSWORD,
    host=DB_HOST,
    port=DB_PORT)

db_migrator = PostgresqlMigrator(db)
