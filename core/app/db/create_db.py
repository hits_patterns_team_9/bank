from core.app.db.connect_db import db
from core.settings import BANK_DATA_DB_NAME

bank_data_db_name = BANK_DATA_DB_NAME

if not db.table_exists(bank_data_db_name):
    with db.connection_context():
        db.execute_sql(f"CREATE DATABASE {bank_data_db_name} OWNER = postgres ENCODING 'UTF8' LC_COLLATE 'en_US.utf8' "
                       f"LC_CTYPE 'en_US.utf8';")
        print(f"База данных '{bank_data_db_name}' успешно создана.")
else:
    print(f"База данных '{bank_data_db_name}' уже существует.")

db.close()