from core.app.db.drop_tables import drop_all_tables
from core.app.db.create_tables import create_all_tables


def recreate_all_tables():
    drop_all_tables()
    create_all_tables()


recreate_all_tables()
