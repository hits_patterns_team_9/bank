from peewee import *
from core.app.models.base_model import BaseModel
from core.app.models.credits_model import Credit


class History(BaseModel):
    credit_id = ForeignKeyField(Credit, field='id', on_delete='cascade')
    operation_name = TextField()
    operation_type = TextField()
    operation_sum = FloatField()

    class Meta:
        table_name = 'histories'


