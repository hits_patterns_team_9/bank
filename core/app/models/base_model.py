from peewee import Model, UUIDField, SQL, DateTimeField
from core.app.db.connect_db import bank_data_db


class BaseModel(Model):
    id = UUIDField(primary_key=True, unique=True,
                   constraints=[SQL('DEFAULT gen_random_uuid()')])
    created_at = DateTimeField(constraints=[SQL('DEFAULT CURRENT_TIMESTAMP')])
    updated_at = DateTimeField(constraints=[SQL('DEFAULT CURRENT_TIMESTAMP')])

    class Meta:
        database = bank_data_db
