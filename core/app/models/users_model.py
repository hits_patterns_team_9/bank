from peewee import *
from core.app.models.base_model import BaseModel


class User(BaseModel):
    user_id = UUIDField(unique=True)

    class Meta:
        table_name = 'users'
