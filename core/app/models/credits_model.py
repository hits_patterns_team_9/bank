from datetime import datetime, timedelta
from peewee import *
from core.app.models.base_model import BaseModel
from core.app.models.users_model import User

expire_future_date = datetime.now() + timedelta(days=365 * 4)


class Credit(BaseModel):
    user_id = ForeignKeyField(User, field='id', backref='credits',
                              constraint_name='fk_credit_user', on_delete='cascade')
    credit_number = TextField(unique=True)
    credit_name = TextField(null=True)
    credit_sum = FloatField(default=0.0)
    credit_type = TextField(default='debit')
    money_type = TextField(default='RUB')
    service_price = IntegerField(default=70)
    withdraw_low_limit_per_day = IntegerField(default=0)
    withdraw_high_limit_per_day = IntegerField(default=150_000)
    withdraw_low_limit_per_month = IntegerField(default=0)
    withdraw_high_limit_per_month = IntegerField(default=1_500_000)
    commission = FloatField(default=1.5)
    interest_rate = FloatField(default=0.0)
    created_date = DateTimeField(constraints=[SQL('DEFAULT CURRENT_TIMESTAMP')])
    expire_date = DateTimeField(constraints=[SQL('DEFAULT (CURRENT_TIMESTAMP + INTERVAL \'4 years\')')])
    additional_conditions = TextField(null=True)

    class Meta:
        table_name = 'credits'
